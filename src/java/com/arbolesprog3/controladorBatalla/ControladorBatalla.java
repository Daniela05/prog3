/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controladorBatalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modeloBatalla.ArbolBinarioB;
import com.arbolesprog3.modeloBatalla.NodoABB;
import com.arbolesprog3.modeloBatalla.Rol;
import com.arbolesprog3.modeloBatalla.TipoBarco;
import com.arbolesprog3.modeloBatalla.Usuario;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author daniela
 */
@Named(value = "conroladorBatallaNaval")
@SessionScoped
public class ControladorBatalla implements Serializable {

    private DefaultDiagramModel model;
    private TipoBarco barco = new TipoBarco();
    private NodoABB nodoBatalla;
    private ArbolBinarioB arbolBB = new ArbolBinarioB();

    private String barcoBuscar;
    private String barcoBuscarPadre;

    private boolean verRegistrar = false;
    private boolean ocultarPanel = true;
    private boolean verConsultar = false;
    private boolean verTablaConsulta = false;
    private Boolean deshabilitarTableros = true;
//    private boolean verRegistrarUsuario = false;
//    private boolean verUsuario=false;

    /**
     * Creates a new instance of ConroladorBatallaNaval
     */
    public ControladorBatalla() {
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }

    public NodoABB getNodoBatalla() {
        return nodoBatalla;
    }

    public void setNodoBatalla(NodoABB nodoBatalla) {
        this.nodoBatalla = nodoBatalla;
    }

    public Boolean getDeshabilitarTableros() {
        return deshabilitarTableros;
    }

    public void setDeshabilitarTableros(Boolean deshabilitarTableros) {
        this.deshabilitarTableros = deshabilitarTableros;
    }

    public ArbolBinarioB getArbolBB() {
        return arbolBB;
    }

    public void setArbolBB(ArbolBinarioB arbolBB) {
        this.arbolBB = arbolBB;
    }

    public String getBarcoBuscar() {
        return barcoBuscar;
    }

    public void setBarcoBuscar(String barcoBuscar) {
        this.barcoBuscar = barcoBuscar;
    }

    public String getBarcoBuscarPadre() {
        return barcoBuscarPadre;
    }

    public void setBarcoBuscarPadre(String barcoBuscarPadre) {
        this.barcoBuscarPadre = barcoBuscarPadre;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public boolean isOcultarPanel() {
        return ocultarPanel;
    }

    public void setOcultarPanel(boolean ocultarPanel) {
        this.ocultarPanel = ocultarPanel;
    }

//    public boolean isVerRegistrarUsuario() {
//        return verRegistrarUsuario;
//    }
//
//    public void setVerRegistrarUsuario(boolean verRegistrarUsuario) {
//        this.verRegistrarUsuario = verRegistrarUsuario;
//    }
//    public boolean isVerUsuario() {
//        return verUsuario;
//    }
//
//    public void setVerUsuario(boolean verUsuario) {
//        this.verUsuario = verUsuario;
//    }
    public boolean isVerConsultar() {
        return verConsultar;
    }

    public void setVerConsultar(boolean verConsultar) {
        this.verConsultar = verConsultar;
    }

    public boolean isVerTablaConsulta() {
        return verTablaConsulta;
    }

    public void setVerTablaConsulta(boolean verTablaConsulta) {
        this.verTablaConsulta = verTablaConsulta;
    }

    //MEtodos del menu Administrador 
    @PostConstruct
    private void iniciar() {

        try {
            arbolBB.adicionarNodo(new TipoBarco("Portaviones", (byte) 3, (byte) 1));
//            arbolBB.adicionarNodo(new TipoBarco("Fragata", (byte)5, (byte) 1));
//            arbolBB.adicionarNodo(new TipoBarco("Acorazado", (byte)4, (byte) 2));
//            arbolBB.adicionarNodo(new TipoBarco("Navio", (byte)2, (byte) 2));

        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

        pintarArbol();
        habilitarTableros();
    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolBB.getRaiz(), model, null, 22, 0);
    }

    private void pintarArbol(com.arbolesprog3.modeloBatalla.NodoABB reco, DefaultDiagramModel model,
            Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 8);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 8);
        }

    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }

    public void guardarTipoBarco() {
        try {
            arbolBB.adicionarNodo(barco);
            barco = new TipoBarco();
            verRegistrar = false;
            ocultarPanel = true;
            pintarArbol();
            deshabilitarTableros = false;
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        ocultarPanel = false;

//        barco = new TipoBarco();
    }

//      public void habilitarRegistrarUsu() {
//        verRegistrarUsuario = true;
//        ocultarPanel = false;
//        
////        barco = new TipoBarco();
//    }
    public void cancelarGuardado() {
        verRegistrar = false;
        ocultarPanel = true;

    }

    public void habilitarConsulta() {
        verConsultar = true;

    }

    public void consultarBarco() {

        verTablaConsulta = true;

    }

    public void cancelarConsultarBarco() {
        verConsultar = false;
        ocultarPanel = true;

    }

//    public void cancelarGuardadoUsu() {
//        verRegistrarUsuario = false;
//        ocultarPanel=true;
//
//    }
    public void borrarDato(TipoBarco borrar) {
        arbolBB.borrarDato(borrar);
        JsfUtil.addSuccessMessage("Barco Eliminado");
        pintarArbol();
    }

    public void podar() {
        arbolBB.podar();
        pintarArbol();
    }

    public String buscarTipoBarco() {
        if (arbolBB.buscarTipoBarco(barcoBuscar) == false) {
            return "El Barco no existe" + barcoBuscar;
        } else if (arbolBB.buscarTipoBarco(barcoBuscar) != false) {
            return "El barco si existe" + barcoBuscar;
        }
        return null;
    }

    public String darPadre(String nombre) {
        if (arbolBB.getRaiz().getDato().getNombre().compareTo(nombre) == 0) {

            return "La raiz no tiene padre";
        }
        TipoBarco padreHoja = arbolBB.buscarPadre(nombre);
        if (padreHoja == null) {

            return "No existe el nombre del barco  ingresado";
        }
        return "EL padre de: " + barcoBuscarPadre + " es: " + padreHoja;
    }

    public List<TipoBarco> buscarBarco() {
        List<TipoBarco> barcosEncontrados = new ArrayList<>();
        buscarBarco(arbolBB.getRaiz(), barcosEncontrados);
//        if (barcosEncontrados.isEmpty()){
//            JsfUtil.addErrorMessage("No se encontró el barco");
//        }

        verTablaConsulta = true;
        return barcosEncontrados;

    }

    private void buscarBarco(NodoABB reco, List<TipoBarco> listado) {
        if (reco != null) {
            if (reco.getDato().getNombre().toUpperCase().equals(this.barcoBuscar.toUpperCase())) {
                listado.add(reco.getDato());
            }

            buscarBarco(reco.getIzquierda(), listado);
            buscarBarco(reco.getDerecha(), listado);
        }
    }

    public void habilitarTableros() {
        if (arbolBB.getRaiz() != null) {
            deshabilitarTableros = false;
        } else {
            deshabilitarTableros = true;
        }
    }

//   public String guardarUsuario() {
//
//        if (arbolBB.guardarUsuario(usuario, rol).length() == 0) {
//            arbolBB.guardarUsuario(usuario, rol);
//            usuario = new Usuario();
//            rol = new Rol();
//            verUsuario=true;
//            JsfUtil.addSuccessMessage("Usuario agredado");
//        } else if (arbolBB.guardarUsuario(usuario, rol).equals("correo")) {
//            JsfUtil.addErrorMessage("El correo: " + usuario.getCorreo() + "\nya se encuentra registro");
//        } else if (arbolBB.guardarUsuario(usuario, rol).equals("rol")) {
//            JsfUtil.addErrorMessage("El rol: " + rol.getNombre() + "\nya se encuentra registro");
//        }
//        return null;
//    }
//   public void verUsuarios(){
//       verUsuario=true;
//   }
}
