/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controladorBatalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modeloCelulares.ArbolABB;
import com.arbolesprog3.modeloBatalla.ArbolBinarioB;
import com.arbolesprog3.modeloBatalla.ArbolNBatalla;
import com.arbolesprog3.modeloBatalla.BarcoPosicionado;
import com.arbolesprog3.modeloBatalla.Coordenada;
import com.arbolesprog3.modeloBatalla.Disparo;
import com.arbolesprog3.modeloBatalla.Estado;
import com.arbolesprog3.modeloBatalla.NodoABB;
import com.arbolesprog3.modeloBatalla.NodoN;
import com.arbolesprog3.modeloBatalla.Rol;
import com.arbolesprog3.modeloBatalla.TipoBarco;
import com.arbolesprog3.modeloBatalla.Usuario;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.flow.SwitchCase;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author daniela
 */
@Named(value = "controladorJuego")
@ApplicationScoped
public class ControladorJuego implements Serializable {

    private DefaultDiagramModel model1;
    private DefaultDiagramModel model2;

    private BarcoPosicionado barcoPos = new BarcoPosicionado();
    private ArbolNBatalla arbolNBatallaJugador1 = new ArbolNBatalla();
    private ArbolNBatalla arbolNBatallaJugador2 = new ArbolNBatalla();
    private ArbolBinarioB arbolBinario = new ArbolBinarioB();
    private ControladorBatalla contAbb = (ControladorBatalla) FacesUtils.
            getManagedBean("conroladorBatallaNaval");
    private NodoN nodoN;
    private NodoABB nodoABB;
    private Usuario administrador;
    private Usuario jugador1;
    private Usuario jugador2;
    private List<Rol> roles;
    private List<Disparo> disparosJugador1 = new ArrayList<>();
    private List<Disparo> disparosJugador2 = new ArrayList<>();
    private Coordenada coordenada = new Coordenada();
    private Rol rol = new Rol();
    private int fila;
    private int columna;
    private int fila1 = 0;
    private int columna1 = 0;
    private int tablero1;
    private int tablero2;
    private String direccion = "seleccione";
    private String estilo;
    private String estado;
    private boolean deshabilitarAsignarJugador1 = true;
    private boolean deshabilitarAsignarJugador2 = true;
    private boolean habilitarTableros = false;
    private boolean turno1 = false;
    private boolean turno2 = true;

    /**
     * Creates a new instance of ControladorJuego
     */
    public ControladorJuego() {
    }

    public BarcoPosicionado getBarcoPos() {
        return barcoPos;
    }

    public void setBarcoPos(BarcoPosicionado barcoPos) {
        this.barcoPos = barcoPos;
    }

    public ArbolNBatalla getArbolNBatallaJugador1() {
        return arbolNBatallaJugador1;
    }

    public void setArbolNBatallaJugador1(ArbolNBatalla arbolNBatallaJugador1) {
        this.arbolNBatallaJugador1 = arbolNBatallaJugador1;
    }

    public ArbolNBatalla getArbolNBatallaJugador2() {
        return arbolNBatallaJugador2;
    }

    public void setArbolNBatallaJugador2(ArbolNBatalla arbolNBatallaJugador2) {
        this.arbolNBatallaJugador2 = arbolNBatallaJugador2;
    }

    public ArbolBinarioB getArbolBinario() {
        return arbolBinario;
    }

    public void setArbolBinario(ArbolBinarioB arbolBinario) {
        this.arbolBinario = arbolBinario;
    }

    public NodoN getNodoN() {
        return nodoN;
    }

    public void setNodoN(NodoN nodoN) {
        this.nodoN = nodoN;
    }

    public NodoABB getNodoABB() {
        return nodoABB;
    }

    public void setNodoABB(NodoABB nodoABB) {
        this.nodoABB = nodoABB;
    }

    public Usuario getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Usuario administrador) {
        this.administrador = administrador;
    }

    public Usuario getJugador1() {
        return jugador1;
    }

    public void setJugador1(Usuario jugador1) {
        this.jugador1 = jugador1;
    }

    public Usuario getJugador2() {
        return jugador2;
    }

    public void setJugador2(Usuario jugador2) {
        this.jugador2 = jugador2;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public ControladorBatalla getContAbb() {
        return contAbb;
    }

    public void setContAbb(ControladorBatalla contAbb) {
        this.contAbb = contAbb;
    }

    public int getTablero1() {
        return tablero1;
    }

    public void setTablero1(int tablero1) {
        this.tablero1 = tablero1;
    }

    public int getTablero2() {
        return tablero2;
    }

    public void setTablero2(int tablero2) {
        this.tablero2 = tablero2;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Disparo> getDisparosJugador1() {
        return disparosJugador1;
    }

    public void setDisparosJugador1(List<Disparo> disparosJugador1) {
        this.disparosJugador1 = disparosJugador1;
    }

    public List<Disparo> getDisparosJugador2() {
        return disparosJugador2;
    }

    public void setDisparosJugador2(List<Disparo> disparosJugador2) {
        this.disparosJugador2 = disparosJugador2;
    }

    public boolean isDeshabilitarAsignarJugador1() {
        return deshabilitarAsignarJugador1;
    }

    public void setDeshabilitarAsignarJugador1(boolean deshabilitarAsignarJugador1) {
        this.deshabilitarAsignarJugador1 = deshabilitarAsignarJugador1;
    }

    public boolean isTurno1() {
        return turno1;
    }

    public void setTurno1(boolean turno1) {
        this.turno1 = turno1;
    }

    public boolean isTurno2() {
        return turno2;
    }

    public void setTurno2(boolean turno2) {
        this.turno2 = turno2;
    }

    public int getFila1() {
        return fila1;
    }

    public void setFila1(int fila1) {
        this.fila1 = fila1;
    }

    public int getColumna1() {
        return columna1;
    }

    public void setColumna1(int columna1) {
        this.columna1 = columna1;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public boolean isDeshabilitarAsignarJugador2() {
        return deshabilitarAsignarJugador2;
    }

    public void setDeshabilitarAsignarJugador2(boolean deshabilitarAsignarJugador2) {
        this.deshabilitarAsignarJugador2 = deshabilitarAsignarJugador2;
    }

    public boolean isHabilitarTableros() {
        return habilitarTableros;
    }

    public void setHabilitarTableros(boolean habilitarTableros) {
        this.habilitarTableros = habilitarTableros;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @PostConstruct
    private void iniciar() {

        llenarRol();
//        try {
//            cargarArbol();
//        } catch (BarcoExcepcion ex) {
//            Logger.getLogger(ControladorJuego.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        pintarArbol();
        administrador = new Usuario("daniela", "123", new Rol((byte) 0, "Administrador"));
        jugador1 = new Usuario("jugador1", "123", roles.get(0));
        jugador2 = new Usuario("jugador2", "123", roles.get(1));

    }

    public int crearTableroJugador1() {
        if (arbolNBatallaJugador1.getCantidadNodos() <= 10 && arbolNBatallaJugador1.getCantidadNodos() != 0) {
            tablero1 = 10;

        } else if (arbolNBatallaJugador1.getCantidadNodos() > 10 && arbolNBatallaJugador1.getCantidadNodos() <= 20) {
            tablero1 = 20;

        } else if (arbolNBatallaJugador1.getCantidadNodos() > 20 && arbolNBatallaJugador1.getCantidadNodos() <= 30) {
            tablero1 = 30;

        }
        return tablero1;
    }

    public int crearTableroJugador2() {

        if (arbolNBatallaJugador2.getCantidadNodos() <= 10 && arbolNBatallaJugador2.getCantidadNodos() != 0) {
            tablero2 = 10;

        } else if (arbolNBatallaJugador2.getCantidadNodos() > 10 && arbolNBatallaJugador2.getCantidadNodos() <= 20) {
            tablero2 = 20;

        } else if (arbolNBatallaJugador2.getCantidadNodos() > 20 && arbolNBatallaJugador2.getCantidadNodos() <= 30) {
            tablero2 = 30;

        }
        return tablero2;
    }

    public void llenarRol() {
        roles = new ArrayList<>();
        roles.add(new Rol((byte) 1, "Jugador - 1"));
        roles.add(new Rol((byte) 2, "Jugador - 2"));

    }

    private void cargarArbolJugador1() throws BarcoExcepcion {

        //Algoritmo
        arbolNBatallaJugador1.adicionarNodo(new BarcoPosicionado(new TipoBarco(jugador1.getCorreo(), (byte) 0, (byte) 0)), null);

        List<BarcoPosicionado> padres = new ArrayList<>();
        padres.add(arbolNBatallaJugador1.getRaiz().getDato());
        adicionarPreOrdenJugador1(contAbb.getArbolBB().getRaiz(), padres, 0);

    }

    private void cargarArbolJugador2() throws BarcoExcepcion {

        //Algoritmo
        arbolNBatallaJugador2.adicionarNodo(new BarcoPosicionado(new TipoBarco(jugador2.getCorreo(), (byte) 0, (byte) 0)), null);

        List<BarcoPosicionado> padres = new ArrayList<>();
        padres.add(arbolNBatallaJugador2.getRaiz().getDato());
        adicionarPreOrdenJugador2(contAbb.getArbolBB().getRaiz(), padres, 0);

    }

    private void adicionarPreOrdenJugador1(NodoABB reco, List<BarcoPosicionado> padres, int contizq) throws BarcoExcepcion {
        if (reco != null) {
            List<BarcoPosicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(reco.getDato());
                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNBatallaJugador1.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenJugador1(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbolBB().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrdenJugador1(reco.getDerecha(), padresNuevos, contizq);
        }
    }

    private void adicionarPreOrdenJugador2(NodoABB reco, List<BarcoPosicionado> padres, int contizq) throws BarcoExcepcion {
        if (reco != null) {
            List<BarcoPosicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(reco.getDato());
                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNBatallaJugador2.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenJugador2(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbolBB().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrdenJugador2(reco.getDerecha(), padresNuevos, contizq);
        }
    }

    public void pintarArbolJugador1() {
        model1 = new DefaultDiagramModel();
        model1.setMaxConnections(-1);
        model1.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model1.setDefaultConnector(connector);

        pintarArbol(arbolNBatallaJugador1.getRaiz(), model1, null, 30, 0);
    }

    public void pintarArbolJugador2() {
        model2 = new DefaultDiagramModel();
        model2.setMaxConnections(-1);
        model2.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model2.setDefaultConnector(connector);

        pintarArbol(arbolNBatallaJugador2.getRaiz(), model2, null, 30, 0);
    }

    private void pintarArbol(NodoN reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));
            }

            model.addElement(elementHijo);
            for (int i = 0; i < reco.getHijos().size(); i++) {
                pintarArbol(reco.getHijos().get(i), model, elementHijo, x - reco.getHijos().size() - 3, y + 8);
                x += 10;

            }

        }
    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel1() {
        return model1;
    }

    public DiagramModel getModel2() {
        return model2;
    }

    public void generarBarcosJuego() {

        try {

            cargarArbolJugador1();
            cargarArbolJugador2();
            JsfUtil.addSuccessMessage("Barcos Cargados con exito");
        } catch (BarcoExcepcion ex) {
            Logger.getLogger(ControladorJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
        pintarArbolJugador1();
        pintarArbolJugador2();
        contAbb.setDeshabilitarTableros(true);
    }

    public void asignarCoordenadasjugador1MenuContext(BarcoPosicionado barcopos) {

        if (validarCoordenadaOcupadaJugador1(fila, columna)) {

            JsfUtil.addErrorMessage("Coordenadas Ocupadas");

        } else if (direccion.equals("Vertical") && (fila + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero1) {
            JsfUtil.addErrorMessage("Filas Fuera de Rango");

        } else if (direccion.equals("Horizontal") && (columna + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero1) {
            JsfUtil.addErrorMessage("columnas Fuera de Rango");
        } else {
            arbolNBatallaJugador1.instanciarCoordenadas(barcopos, direccion, fila, columna);
        }

    }

    public void asignarCoordenadasjugador2MenuContext(BarcoPosicionado barcopos) {

        if (validarCoordenadaOcupadaJugador2(fila, columna)) {

            JsfUtil.addErrorMessage("Coordenadas Ocupadas");

        } else if (direccion.equals("Vertical") && (fila + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero2) {
            JsfUtil.addErrorMessage("Filas Fuera de Rango");

        } else if (direccion.equals("Horizontal") && (columna + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero2) {
            JsfUtil.addErrorMessage("columnas Fuera de Rango");
        } else {
            arbolNBatallaJugador2.instanciarCoordenadas(barcopos, direccion, fila, columna);
        }

    }

    public boolean verififcarDisparoJugador1(int fila, int columna) {
        if (disparosJugador2 != null) {
            for (Disparo disp : disparosJugador2) {
                if (disp.getFila() == fila && disp.getColumna() == columna) {
                    return true;
                }
            }

        }

        return false;
    }

    public void dispararJug1() {
        JsfUtil.addSuccessMessage("Has disparado en la fila: " + fila1 + " columna: " + columna1);
        disparosJugador1.add(new Disparo(fila1, columna1));
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarImpacto(fila1, columna1)) {
                if (miBarquito.verificarEstadoBarco()) {
                    if (miBarquito.getEstado().getDescripcion().equals("Destruido")) {
                        estado = "Destruido";
                        JsfUtil.addSuccessMessage("El " + miBarquito.getTipobarco().getNombre() + "ha sido destruido");
                    } else if (miBarquito.getEstado().getDescripcion().equals("Tocado")) {
                        estado = "Tocado";
                        JsfUtil.addSuccessMessage("Haz acertado");

                    } else {
                        estado = "Agua";
                        JsfUtil.addSuccessMessage("Disparo Fallido: \n Turno del jugador 2 ");
                        turno1 = true;
                        turno2 = false;
                    }
                }
            }
        }
    }

    public void dispararJug1_x(int fila1, int columna1) {

        if (turno1) {
            JsfUtil.addSuccessMessage("Has disparado en la fila: " + fila1 + " columna: " + columna1);
            disparosJugador1.add(new Disparo(fila1, columna1));
            for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
                if (miBarquito.validarImpacto(fila1, columna1)) {
                    if (miBarquito.verificarEstadoBarco()) {
                        if (miBarquito.getEstado().getDescripcion().equals("Destruido")) {
                            estado = "Destruido";
                            JsfUtil.addSuccessMessage("El " + miBarquito.getTipobarco().getNombre() + "ha sido destruido");
                        } else if (miBarquito.getEstado().getDescripcion().equals("Tocado")) {
                            estado = "Tocado";
                            JsfUtil.addSuccessMessage("Haz acertado");
                        }
                    }
                } else {
                    JsfUtil.addSuccessMessage("Disparo Fallido: \n Turno del jugador 2 ");
                    turno1 = false;
                }
            }
        } else {
            JsfUtil.addSuccessMessage("No es su turno");
        }

    }

    public boolean verififcarDisparoJugador2(int fila, int columna) {
        if (disparosJugador1 != null) {
            for (Disparo disp : disparosJugador1) {
                if (disp.getFila() == fila && disp.getColumna() == columna) {
                    return true;
                }
            }

        }

        return false;
    }

    public void dispararJug2() {
        JsfUtil.addSuccessMessage("Has disparado en la fila: " + fila1 + " columna: " + columna1);
        disparosJugador2.add(new Disparo(fila1, columna1));
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarImpacto(fila1, columna1)) {
                if (miBarquito.verificarEstadoBarco()) {
                    if (miBarquito.getEstado().getDescripcion().equals("Destruido")) {
                        estado = "Destruido";
                        JsfUtil.addSuccessMessage("El" + miBarquito.getTipobarco().getNombre() + "ha sido destruido");
                    } else if (miBarquito.getEstado().getDescripcion().equals("Tocado")) {
                        estado = "Tocado";
                        JsfUtil.addSuccessMessage("Haz acertado");
                    } else {
                        estado = "Agua";
                        JsfUtil.addSuccessMessage("Disparo Fallido: \n Turno del jugador 2 ");
                        turno2 = true;
                        turno1 = false;
                    }
                }
            }
        }

    }

    public String pintarBarcosJugador1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                switch (miBarquito.getTipobarco().getNombre()) {

                    case "Portaviones":
                        return "width: 80px; heigth: 80px; background-color: orange;";

                    case "Acorazado":
                        return "width: 80px; heigth: 80px; background-color: brown;";

                    case "Fragata":
                        return "width: 80px; heigth: 80px; background-color: yellow;";

                    case "Navio":
                        return "width: 80px; heigth: 80px; background-color: pink;";

                    default:
                        return "width: 80px; heigth: 80px; background-color: white;";

                }
//               if(miBarquito.getTipobarco().getNombre().compareTo("Buque")==0)
//               {

//               }
//               else if(miBarquito.getTipobarco().getNombre().compareTo("Acorazado")==0)
//               {
//                   return "width: 72px; heigth: 72px; background-color: blue;";
//               }
            }
        }
        return "width: 80px; heigth: 80px;";
    }

    public String obtenerEsiloDeCoordenada1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenadaJuego(fila, columna)) {
                return "width: 80px; heigth: 80px; background-color: red;";

            }

        }
        return "width: 80px; heigth: 80px;";
    }

    public String pintarBarcosJugador2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                switch (miBarquito.getTipobarco().getNombre()) {

                    case "Portaviones":
                        return "width: 80px; heigth: 80px; background-color: orange;";

                    case "Acorazado":
                        return "width: 80px; heigth: 80px; background-color: brown;";

                    case "Fragata":
                        return "width: 80px; heigth: 80px; background-color: yellow;";

                    case "Navio":
                        return "width: 80px; heigth: 80px; background-color: pink;";

                    default:
                        return "width: 80px; heigth: 80px; background-color: white;";

                }
//               if(miBarquito.getTipobarco().getNombre().compareTo("Buque")==0)
//               {

//               }
//               else if(miBarquito.getTipobarco().getNombre().compareTo("Acorazado")==0)
//               {
//                   return "width: 72px; heigth: 72px; background-color: blue;";
//               }
            }
        }
        return "width: 80px; heigth: 80px;";
    }

    public String obtenerEsiloDeCoordenada2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenadaJuego(fila, columna)) {
                return "width: 80px; heigth: 80px; background-color: red;";

            }

        }
        return "width: 80px; heigth: 80px;";
    }

    public boolean validarCoordenadaOcupadaJugador1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                return true;
            }

        }

        return false;
    }

    public boolean validarCoordenadaOcupadaJugador2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                return true;
            }

        }

        return false;
    }

    public void validarTableroLLenoJugador1() {
        for (BarcoPosicionado bar : arbolNBatallaJugador1.listarBarcos()) {
            if (bar.getCoordenadas().isEmpty()) {
                JsfUtil.addErrorMessage("Falta posicionar el barco" + bar);
            }
        }
        if (arbolNBatallaJugador1.listarBarcos().isEmpty()) {
            JsfUtil.addSuccessMessage("Barcos posicionados");
            deshabilitarAsignarJugador1 = false;
        }
    }

    public void validarTableroLLenoJugador2() {
        for (BarcoPosicionado bar : arbolNBatallaJugador2.listarBarcos()) {
            if (bar.getCoordenadas().isEmpty()) {
                JsfUtil.addErrorMessage("Falta posicionar el barco" + bar);
            }
        }
        if (arbolNBatallaJugador2.listarBarcos().isEmpty()) {
            JsfUtil.addSuccessMessage("Barcos posicionados");
            deshabilitarAsignarJugador2 = false;
        }
    }

}
