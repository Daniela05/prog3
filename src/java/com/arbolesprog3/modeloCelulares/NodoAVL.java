/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class NodoAVL implements Serializable{
    
    private Celular dato;
    private NodoAVL izquierda;
    private NodoAVL derecha;
    private int altura ;
    

    //constructor

    public NodoAVL() {
    }
    
    
    public NodoAVL(Celular dato) {
        this.dato = dato;
        altura = 1;
        
    }
 
    //Getters and Setters
    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public NodoAVL getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoAVL izquierda) {
        this.izquierda = izquierda;
    }

    public NodoAVL getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoAVL derecha) {
        this.derecha = derecha;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    //metodos booleanos
     public boolean isHoja() {
        return izquierda == null && derecha == null;
    }

    public boolean isLleno() {
        return izquierda != null && derecha != null;
    }

    public boolean esVacio() {
        return izquierda == null && derecha == null;
    }
    
    
     public void actualizarAltura() {
        if (esVacio()) {
            altura =1;
        }
        int alturaIzq = 0;
        int alturaDer = 0;
        if (izquierda != null) {
            alturaIzq = izquierda.getAltura();
        }
        if (derecha != null) {
            alturaDer = derecha.getAltura();
        }
        altura=Math.max(alturaIzq, alturaDer) + 1;
    }

    public int obtenerFactorEquilibrio() {
        int alturaIzq = 0;
        int alturaDer = 0;
        if (izquierda != null) {
            alturaIzq = izquierda.getAltura();
        }
        if (derecha != null) {
            alturaDer = derecha.getAltura();
        }

        return alturaDer - alturaIzq;
    }

    @Override
    public String toString() {
        return  dato.getImei() + "," +  altura ;
    }

    
    
    
    
}
