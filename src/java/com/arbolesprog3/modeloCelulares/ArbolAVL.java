/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.sun.org.apache.bcel.internal.generic.BALOAD;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniela
 */
public class ArbolAVL implements Serializable {

    private NodoAVL raiz;
    private int cantidadNodos;
    private String[] niveles;
    private int altura;

    //Constructor vacio
    public ArbolAVL() {
    }

    //Getters and setters
    public NodoAVL getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoAVL raiz) {
        this.raiz = raiz;
    }

    public void isLleno() throws CelularExcepcion {
        if (raiz == null) {
            throw new CelularExcepcion("El árbol está vacío");
        }
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public String[] getNiveles() {
        return niveles;
    }

    public void setNiveles(String[] niveles) {
        this.niveles = niveles;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

//Metodos del arbol AVL
    public int obtenerFE(NodoAVL pivote) {
        if (pivote == null) {
            return -1;
        } else {
            return pivote.getAltura();
        }
    }

    public void adicionarNodo(Celular dato, NodoAVL ubicacion) throws CelularExcepcion {
        if (raiz == null) {
            raiz = new NodoAVL(dato);
            cantidadNodos++;

        } else {

            if (dato.getImei().compareTo(ubicacion.getDato().getImei()) < 0) {
                if (ubicacion.getIzquierda() == null) {
                    ubicacion.setIzquierda(new NodoAVL(dato));
                    cantidadNodos++;

                } else {
                    adicionarNodo(dato, ubicacion.getIzquierda());
                }
            } else if (dato.getImei().compareTo(ubicacion.getDato().getImei()) > 0) {
                if (ubicacion.getDerecha() == null) {
                    ubicacion.setDerecha(new NodoAVL(dato));
                    cantidadNodos++;
                } else {
                    adicionarNodo(dato, ubicacion.getDerecha());
                }
            } else {
                throw new CelularExcepcion("No se puede repetir");
            }

            ubicacion.actualizarAltura();
            balancear(ubicacion);

        }
    }

    public List<Celular> inOrden() throws CelularExcepcion {
        isLleno();
        ArrayList l = new ArrayList();
        inOrden(raiz, l);
        return l;
    }

    private void inOrden(NodoAVL reco, ArrayList l) {
        if (reco != null) {
            inOrden(reco.getIzquierda(), l);
            l.add(reco.getDato());
            inOrden(reco.getDerecha(), l);
        }
    }

    public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    public void recorrerPreOrden(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listado);
            recorrerPreOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPostOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listado);
            recorrerPostOrden(reco.getDerecha(), listado);
            listado.add(reco.getDato());
        }
    }

//    public void llenarArbol(String datos) throws CelularExcepcion {
//        String[] arrayDatos = datos.split(",");
//        for (String cadena : arrayDatos) {
//            adicionarNodo(Integer.parseInt(cadena), raiz);
//        }
//
//    }
    public void rotarSimple(NodoAVL principal, boolean sentido) {
        NodoAVL temp = new NodoAVL(principal.getDato());
        if (!sentido) {
            principal.setDato(principal.getIzquierda().getDato());
            principal.setDerecha(new NodoAVL(temp.getDato()));
            principal.setIzquierda(principal.getIzquierda().getIzquierda());
            principal.getIzquierda().actualizarAltura();
            principal.actualizarAltura();
        } else {
            principal.setDato(principal.getDerecha().getDato());
            principal.setIzquierda(new NodoAVL(temp.getDato()));
            principal.setDerecha(principal.getDerecha().getDerecha());
            principal.getDerecha().actualizarAltura();
            principal.actualizarAltura();

        }
    }

    public void rotarSimpleNuevo(NodoAVL princ, boolean sentido) {

        if (!sentido) {
            if (princ.getDerecha() != null) {
                NodoAVL nodo = princ.getDerecha();
                princ.setDerecha(new NodoAVL(princ.getDato()));
                princ.getDerecha().setDerecha(nodo);
            } else {
                princ.setDerecha(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getIzquierda().getDato());
            if (princ.getIzquierda().getDerecha() != null) {
                NodoAVL nodo = princ.getIzquierda().getDerecha();
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
                princ.getIzquierda().setDerecha(nodo);
            } else {
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
            }
            princ.getDerecha().actualizarAltura();
        } else {
            if (princ.getIzquierda() != null) {
                NodoAVL nodo = princ.getIzquierda();
                princ.setIzquierda(new NodoAVL(princ.getDato()));
                princ.getIzquierda().setIzquierda(nodo);
            } else {
                princ.setIzquierda(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getDerecha().getDato());
            if (princ.getDerecha().getIzquierda() != null) {
                NodoAVL nodo = princ.getDerecha().getIzquierda();
                princ.setDerecha(princ.getDerecha().getDerecha());
                princ.getDerecha().setIzquierda(nodo);
            } else {
                princ.setDerecha(princ.getDerecha().getDerecha());
            }
            princ.getIzquierda().actualizarAltura();
        }
        princ.actualizarAltura();
    }

    public void balancear(NodoAVL principal) {
        if (!principal.esVacio()) {
//        if (principal != null) {
            int fe = principal.obtenerFactorEquilibrio();
            boolean signo = true;
            if (fe < 0) {
                signo = false;
                fe = fe * -1;
            }
            if (fe == 2) {
                //Esta desequilibrado
                //hacia donde
                if (signo) {
                    //Desequilibrio a la derecha
                    //Valido desequilibrio simple a la izq
                    if (principal.getDerecha().obtenerFactorEquilibrio() > 0) {
                        //Desequilibrio simple - Rotacion simple
                        rotarSimpleNuevo(principal, signo);

                    } else {
                        //Desequilibrio doble - Rotación doble
                        rotarSimpleNuevo(principal.getDerecha(), false);
                        rotarSimpleNuevo(principal, true);
                    }
                } else {
                    //Desequilibrio a la izquierda
                    //Valido desequilibrio simple a la izq
                    if (principal.getIzquierda().obtenerFactorEquilibrio() < 0) {
                        rotarSimpleNuevo(principal, signo);
                    } else {
                        //Tengo un zig zag
                        //rotar doble
                        rotarSimpleNuevo(principal.getIzquierda(), true);
                        rotarSimpleNuevo(principal, false);
                    }
                }
            }
        }

    }

    //Metodos de rotaciones alternativas
    public NodoAVL rotarSimpleIzquierda(NodoAVL pivote) {
        NodoAVL auxiliar = pivote.getIzquierda();
        pivote.setIzquierda(auxiliar.getDerecha());
        auxiliar.setDerecha(pivote);
//        pivote.setAltura(Math.max(obtenerFE(pivote.getIzquierda()), obtenerFE(pivote.getDerecha())) + 1);
//        auxiliar.setAltura(Math.max(obtenerFE(auxiliar.getIzquierda()), obtenerFE(auxiliar.getDerecha())) + 1);
        pivote.actualizarAltura();
        auxiliar.actualizarAltura();
        return auxiliar;
    }

    public NodoAVL rotarSimpleDerecha(NodoAVL pivote) {
        NodoAVL auxiliar = pivote.getDerecha();
        pivote.setDerecha(auxiliar.getIzquierda());
        auxiliar.setIzquierda(pivote);
//        pivote.setAltura(Math.max(obtenerFE(pivote.getIzquierda()), obtenerFE(pivote.getDerecha())) + 1);
//        auxiliar.setAltura(Math.max(obtenerFE(auxiliar.getIzquierda()), obtenerFE(auxiliar.getDerecha())) + 1);
        pivote.actualizarAltura();
        auxiliar.actualizarAltura();
        return auxiliar;
    }

    public NodoAVL rotarDobleIzquierda(NodoAVL pivote) {
        NodoAVL auxiliar;
        pivote.setIzquierda(rotarSimpleDerecha(pivote.getIzquierda()));
        auxiliar = rotarSimpleIzquierda(pivote);
        return auxiliar;

    }

    public NodoAVL rotarDobleDerecha(NodoAVL pivote) {
        NodoAVL auxiliar;
        pivote.setDerecha(rotarSimpleIzquierda(pivote.getDerecha()));
        auxiliar = rotarSimpleDerecha(pivote);
        return auxiliar;

    }
//Metodo Insertar balanceado Alternativo

    private NodoAVL insertarNodoBalanceado(NodoAVL nuevo, NodoAVL pivote) throws CelularExcepcion {
        NodoAVL nuevoPadre = pivote;
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) < 0) {
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                pivote.setIzquierda(insertarNodoBalanceado(nuevo, pivote.getIzquierda()));
//                if (obtenerFE(pivote.getIzquierda()) - obtenerFE(pivote.getDerecha()) == 2) {
                    if (pivote.obtenerFactorEquilibrio() == -2) {
                    if (nuevo.getDato().getImei().compareTo(pivote.getIzquierda().getDato().getImei()) < 0) {
                        nuevoPadre = rotarSimpleIzquierda(pivote);
                    } else {
                        nuevoPadre = rotarDobleIzquierda(pivote);
                    }
                }
            }
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) > 0) {
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);

            } else {
                pivote.setDerecha(insertarNodoBalanceado(nuevo, pivote.getDerecha()));
//                if (obtenerFE(pivote.getDerecha()) - obtenerFE(pivote.getIzquierda()) == 2) {
                     if (pivote.obtenerFactorEquilibrio() == 2){
                    if (nuevo.getDato().getImei().compareTo(pivote.getDerecha().getDato().getImei()) > 0) {
                        nuevoPadre = rotarSimpleDerecha(pivote);

                    } else {
                        nuevoPadre = rotarDobleDerecha(pivote);
                    }
                }
            }
        } else {
            throw new CelularExcepcion("No se puede Repetir Dato");
        }
//        if ((pivote.getIzquierda() == null) && (pivote.getDerecha() != null)) {
//            pivote.setAltura(pivote.getDerecha().getAltura() + 1);
//        } else if ((pivote.getDerecha() == null) && (pivote.getIzquierda() != null)) {
//            pivote.setAltura(pivote.getIzquierda().getAltura() + 1);
//        } else {
//            pivote.setAltura(Math.max(obtenerFE(pivote.getIzquierda()), obtenerFE(pivote.getDerecha())) + 1);
//        }
        nuevoPadre.actualizarAltura();
        pivote.actualizarAltura();
        return nuevoPadre;
    }

    public void insertarNodo(Celular dato) throws CelularExcepcion {
        NodoAVL nuevo = new NodoAVL(dato);
        if (raiz == null) {
            raiz = nuevo;
            cantidadNodos++;
        } else {

            raiz = insertarNodoBalanceado(nuevo, raiz);
            cantidadNodos++;
        }
    }

    public double sumarPrecios() {

        return sumarPostOrden(raiz);
    }

    private double sumarPostOrden(NodoAVL reco) {
        double suma = 0;
        if (reco != null) {

            suma += sumarPostOrden(reco.getIzquierda());
            suma += sumarPostOrden(reco.getDerecha());
            suma += reco.getDato().getPrecio();

        }
        return suma;
    }

    public int sumarInOrdenRecursivo(NodoAVL reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidad();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;

    }

    public double calcularPromedioPrecios() {
        return sumarPrecios() / (double) cantidadNodos;
    }

    public int contarCelularesxLetra(String letra) {
        return contarCelularesXMarca(raiz, letra);
    }

    private int contarCelularesXMarca(NodoAVL pivote, String letra) {
        int cont = 0;
        if (pivote != null) {
            if (pivote.getDato().getMarca().getDescripcion().toUpperCase().contains(letra.toUpperCase())) {
                cont++;
            }
            cont += contarCelularesXMarca(pivote.getIzquierda(), letra);
            cont += contarCelularesXMarca(pivote.getDerecha(), letra);
            return cont;
        }
        return cont;
    }

    private NodoAVL buscarMin(NodoAVL pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public int cantidadNodosHoja() {

        return cantidadNodosHoja(raiz);
    }

    private int cantidadNodosHoja(NodoAVL pivote) {
        int cant = 0;
        if (pivote != null) {
            if (pivote.getIzquierda() == null && pivote.getDerecha() == null) {
                cant++;
            }
            cant += cantidadNodosHoja(pivote.getIzquierda());
            cant += cantidadNodosHoja(pivote.getDerecha());
            return cant;
        }
        return 0;
    }

    public int retornarAltura() {
        return raiz.getAltura();
    }

    public Celular menorValor() {
        NodoAVL pivote = raiz;
        if (raiz != null) {

            while (pivote.getIzquierda() != null) {
                pivote = pivote.getIzquierda();
            }
        }
        return pivote.getDato();
    }

    public Celular mayorValor() {
        NodoAVL pivote = raiz;
        if (raiz != null) {
            while (pivote.getDerecha() != null) {
                pivote = pivote.getDerecha();
            }
        }
        return pivote.getDato();
    }

    public void borrarMenor() {

        if (raiz != null) {
            if (raiz.getIzquierda() == null) {
                raiz = raiz.getDerecha();
            } else {
                NodoAVL anterior = raiz;
                NodoAVL reco = raiz.getIzquierda();
                while (reco.getIzquierda() != null) {
                    anterior = reco;
                    reco = reco.getIzquierda();
                }

                anterior.setIzquierda(reco.getDerecha());
                cantidadNodos--;

                reco.actualizarAltura();
            }
        }

    }

    public void borrarMayor() {

        if (raiz != null) {
            if (raiz.getDerecha() == null) {
                raiz = raiz.getIzquierda();
            } else {
                NodoAVL anterior = raiz;
                NodoAVL reco = raiz.getDerecha();
                while (reco.getDerecha() != null) {
                    anterior = reco;
                    reco = reco.getDerecha();
                }

                anterior.setDerecha(reco.getIzquierda());
                cantidadNodos--;

                reco.actualizarAltura();
            }
        }

    }

    public NodoAVL borrarDato(String imei) throws CelularExcepcion {
        NodoAVL borrar = new NodoAVL(raiz.getDato());
        borrar.getDato().setImei(imei);
        NodoAVL nodoAVL = borrarDato(raiz, borrar);
        this.setRaiz(nodoAVL);

        return nodoAVL;

    }

    private NodoAVL borrarDato(NodoAVL pivote, NodoAVL borrarImei) throws CelularExcepcion {
        NodoAVL nuevoPadre = pivote;
        if (pivote == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = pivote.getDato().getImei().compareTo(borrarImei.getDato().getImei());
        if (compara > 0) {
            pivote.setIzquierda(borrarDato(pivote.getIzquierda(), borrarImei));
            if (pivote.obtenerFactorEquilibrio() == 2) {
                if (borrarImei.getDato().getImei().compareTo(pivote.getIzquierda().getDato().getImei()) < 0) {
                    nuevoPadre = rotarSimpleIzquierda(pivote);
                } else {
                    nuevoPadre = rotarDobleIzquierda(pivote);
                }
            }
        } else if (compara < 0) {
            pivote.setDerecha(borrarDato(pivote.getDerecha(), borrarImei));
            if (pivote.obtenerFactorEquilibrio() == 2) {
                if (borrarImei.getDato().getImei().compareTo(pivote.getIzquierda().getDato().getImei()) < 0) {
                    nuevoPadre = rotarSimpleDerecha(pivote);
                } else {
                    nuevoPadre = rotarDobleDerecha(pivote);
                }
            }
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {

                NodoAVL cambiar = buscarMin(pivote.getDerecha());
                Celular aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarDato(pivote.getDerecha(), borrarImei));

                cantidadNodos--;
            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
                cantidadNodos--;

            }
        }

        return pivote;
    }

    public boolean buscarCelular(String imeiBuscar) {
        return (buscarCelular(this.raiz, imeiBuscar));

    }

    private boolean buscarCelular(NodoAVL pivote, String imeiBuscar) {

        if (pivote == null) {

            return false;

        }

        int compara = ((Comparable) pivote.getDato().getImei()).compareTo(imeiBuscar);
        if (compara > 0) {
            return (buscarCelular(pivote.getIzquierda(), imeiBuscar));
        } else if (compara < 0) {
            return (buscarCelular(pivote.getDerecha(), imeiBuscar));
        } else {

            return true;
        }
    }

    public List<Celular> listarHojas() {
        ArrayList hojas = new ArrayList();
        listarHojas(raiz, hojas);
        return hojas;
    }

    private void listarHojas(NodoAVL pivote, ArrayList hojas) {
        if (pivote != null) {
            if (this.esHoja(pivote)) {
                hojas.add(pivote.getDato());
            }
            listarHojas(pivote.getIzquierda(), hojas);
            listarHojas(pivote.getDerecha(), hojas);
        }

    }

    public Celular buscarPadre(String imei) {
        if (imei.equals("") || raiz == null) {
            return null;
        }
        NodoAVL padre = buscarPadre(raiz, imei);
        if (padre == null) {
            return null;
        }
        return padre.getDato();
    }

    private NodoAVL buscarPadre(NodoAVL pivote, String imei) {
        if (pivote == null) {
            return null;
        }
        if ((pivote.getIzquierda() != null && pivote.getIzquierda().getDato().getImei().compareTo(imei) == 0)
                || (pivote.getDerecha() != null && pivote.getDerecha().getDato().getImei().compareTo(imei) == 0)) {
            return (pivote);
        }
        NodoAVL nodo = buscarPadre(pivote.getIzquierda(), imei);
        if (nodo == null) {
            return (buscarPadre(pivote.getDerecha(), imei));
        } else {
            return nodo;
        }
    }

    public ArrayList impNiveles() {
        ArrayList niveles = new ArrayList();
        impNiveles(raiz, 1, niveles);
        return niveles;
    }

    private void impNiveles(NodoAVL pivote, int nivel, ArrayList niveles) {
        if (pivote != null) {
            impNiveles(pivote.getIzquierda(), nivel + 1, niveles);
            niveles.add(pivote.getDato() + " Nivel: (" + nivel + ") ");
            impNiveles(pivote.getDerecha(), nivel + 1, niveles);
        }
    }

    public ArrayList imprimirNivel() {
        niveles = new String[altura + 1];
        ArrayList nivel = new ArrayList();
        imprimirNivel(raiz, 0);
        for (int i = 0; i < niveles.length; i++) {
            nivel.add(niveles[i] + " ");

        }
        return nivel;
    }

    public void imprimirNivel(NodoAVL pivote, int nivel2) {
        if (pivote != null) {
            niveles[nivel2] = pivote.getDato() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
            imprimirNivel(pivote.getDerecha(), nivel2 + 1);
            imprimirNivel(pivote.getIzquierda(), nivel2 + 1);
        }
    }

    protected boolean esHoja(NodoAVL pivote) {
        return (pivote != null && pivote.getIzquierda() == null && pivote.getDerecha() == null);
    }

    public void podar() {
        podar(raiz);
    }

    private void podar(NodoAVL pivote) {
        if (pivote == null) {
            return;
        }
        if (this.esHoja(pivote.getIzquierda())) {
            pivote.setIzquierda(null);
            cantidadNodos--;
        }
        if (this.esHoja(pivote.getDerecha())) {
            pivote.setDerecha(null);
            cantidadNodos--;
        }
        podar(pivote.getIzquierda());
        podar(pivote.getDerecha());
        balancear(pivote);
        pivote.actualizarAltura();
    }

}
