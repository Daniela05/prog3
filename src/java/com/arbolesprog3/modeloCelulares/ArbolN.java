/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import com.arbolesprog3.utilidades.JsfUtil;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniela
 */
public class ArbolN implements Serializable {

    NodoN raiz;
    int cantidadNodos;

    public ArbolN() {
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public boolean esHoja(NodoN nodo) {
        if (nodo.equals(raiz)) {
            return false;

        }

        return nodo.getHijos() == null;

    }

    public NodoN buscarNodoxImei(String imei) {
        if (raiz != null) {
            return buscarNodoxImei(imei, raiz);
        }
        return null;
    }

    private NodoN buscarNodoxImei(String imei, NodoN pivote) {
        if (pivote.getDato().getImei().compareTo(imei) == 0) {
            return pivote;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarNodoxImei(imei, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado;
                }
            }
        }
        return null;
    }

    public NodoN buscarPadre(String imei) {
        if (raiz.getDato().getImei().compareTo(imei) == 0) {
            return null;
        }
        return buscarPadre(imei, raiz);
    }

    private NodoN buscarPadre(String imei, NodoN pivote) {
        for (NodoN hijo : pivote.getHijos()) {
            if (hijo.getDato().getImei().compareTo(imei) == 0) {
                return pivote;
            } else {
                NodoN padreBuscado = buscarPadre(imei, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    public void adicionarNodo(Celular dato, Celular padre) {
        if (raiz == null) {
            raiz = new NodoN(dato);
        } else {
            NodoN padreEncontrado = buscarNodoxImei(padre.getImei());
            if (padreEncontrado != null) {
                padreEncontrado.getHijos().add(new NodoN(dato));
            }

        }
        cantidadNodos++;
    }
    
    public void adicionarNodoxCodigo(Celular dato, Celular padre)  {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;
    }

    public boolean adicionarNodoxCodigo(Celular dato, Celular padre, NodoN pivote)  {
        // boolean adicionado=false;
        if (pivote.getDato().getCodigo() == padre.getCodigo()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public int obtenerTamaño(NodoN nodo, ArbolN arbol) {
        if (nodo != null) {
            cantidadNodos++;
            for (NodoN tmp : nodo.hijos) {
                obtenerTamaño(tmp, arbol);

            }
            return cantidadNodos;
        }
        return 0;
    }

    public List<Celular> listarCelularesN() {
        List<Celular> listaCelulares = new ArrayList<>();
        listaCelulares.add(raiz.getDato());
        listarCelularesN(raiz, listaCelulares);
        return listaCelulares;
    }

    private void listarCelularesN(NodoN nodo, List<Celular> celulares) {
        for (NodoN agregar : nodo.getHijos()) {
            celulares.add(agregar.getDato());
            listarCelularesN(agregar, celulares);
        }
    }

//     public NodoABB borrarDato(String imei) {
//
//        NodoABB nodoB = borrarDato(raiz, imei);
//        this.setRaiz(nodoB);
//        return nodoB;
//
//    }
//
//    private NodoABB borrarDato(NodoABB pivote, String imei) {
//        if (pivote == null) {
//            return null;//<--Dato no encontrado		
//        }
//        int compara = pivote.getDato().getImei().compareTo(imei);
//        if (compara > 0) {
//            pivote.setIzquierda(borrarDato(pivote.getIzquierda(), imei));
//        } else if (compara < 0) {
//            pivote.setDerecha(borrarDato(pivote.getDerecha(), imei));
//        } else {
//            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
//
//                NodoABB cambiar = buscarMin(pivote.getDerecha());
//                Celular aux = cambiar.getDato();
//                cambiar.setDato(pivote.getDato());
//                pivote.setDato(aux);
//                pivote.setDerecha(borrarDato(pivote.getDerecha(), imei));
//            } else {
//                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
//                cantidadNodos--;
//            }
//        }
//        return pivote;
//    }
//    public void eliminarDato( String imei) {
//         NodoN elimin = buscarPadrexImei(imei);
//        if (elimin == null) {
//            JsfUtil.addErrorMessage("No se encuentra el dato indicado");
//        }else if (elimin.getHijos().isEmpty()){
//            
//           elimin.dato=null;
//            
//        }else {
//            for (NodoN hoja:elimin.getHijos()){
//                if(hoja.getHijos().isEmpty()){
//                    NodoN cambiar = hoja;
//                    Celular aux = cambiar.getDato();
//                    cambiar.setDato(elimin.getDato());
//                    elimin.setDato(aux);
//                    eliminarDato(imei);
//                   
//                }
//                
//        }
//            
//                
//            }
//                
//        
//
//    }
    public boolean eliminarNodo(String id) {
        NodoN padre = buscarPadre(id);
        if (padre != null) {
            int cont = 0;
            for (NodoN hijo : padre.getHijos()) {
                if (hijo.getDato().getImei().compareTo(id) == 0) {
                    padre.getHijos().addAll(hijo.getHijos());

                    break;
                }
                cont++;
            }
            padre.getHijos().remove(cont);
            cantidadNodos--;
            return true;
        }
        return false;
    }

    public boolean eliminarNodoXMarca(String marca) {
        NodoN padre = raiz;
        if (raiz.getDato().getMarca().getDescripcion().compareTo(marca) == 0 && raiz.getHijos().isEmpty()) {
            for (int i = 0; i < raiz.getHijos().size(); i++) {
                if (raiz.getHijos().get(i).getDato().getMarca().getDescripcion().compareTo(marca) != 0) {
                    raiz.setDato(raiz.getHijos().get(i).getDato());
                    return true;
                }
            }
        }
        if (padre != null) {
            int cont = 0;
            for (NodoN hijo : padre.getHijos()) {
                if (hijo.getDato().getMarca().getDescripcion().compareTo(marca) == 0) {
                    padre.getHijos().addAll(hijo.getHijos());

                    break;
                }
                cont++;
            }
            padre.getHijos().remove(cont);
            cantidadNodos--;
            return true;
        }
        return false;
    }

    public int darAltura(NodoN nodo) {
        int maxAltura = 0, aux;
        if (nodo == null) {
            return maxAltura;
        }
        for (int i = 0; i < nodo.getHijos().size(); i++) {
            aux = darAltura(nodo.getHijos().get(i));
            if (aux > maxAltura) {
                maxAltura = aux;
            }
        }
        return maxAltura + 1;
    }

    public int retornalNivel(NodoN nodo, String imei, int nivel) {
        int aux = 0;
        if (nodo == null) {
            return -1;
        } else if (nodo.getDato().getImei().equals(imei)) {
            return nivel;

        } else {
            for (int i = 0; i < nodo.getHijos().size(); i++) {
                aux = retornalNivel(nodo.getHijos().get(i), imei, nivel + 1);
                if (aux != -1) {
                    return aux;
                }
            }
        }
        return -1;
    }
    public List<NodoN> imprimirNodosxNivel(int nivel){
        List<NodoN> listaNiveles=new ArrayList<>();
        imprimirNodosxNivel(raiz, nivel, listaNiveles);
        return listaNiveles;
    }

    private void imprimirNodosxNivel(NodoN nodo, int nivel, List<NodoN> listado) {
        int niv = 0;
        if (nodo != null) {
            if (nivel == 0) {
                listado.add(nodo);
            } else {
                for (NodoN nod : nodo.getHijos()) {
                    if(niv==nivel){
                        listado.add(nod);
                         niv++;
                    }
                    
                    imprimirNodosxNivel(nod, nivel - 1, listado);
                   
                }

            }

        }

    }

    public static void main(String[] args) {

//        ArbolN arbol = new ArbolN();
//        arbol.adicionarNodo(new Celular("yo", null, null, null, null, 0, (byte) 0), null);
//        arbol.adicionarNodo(new Celular("1WE", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "yo");
//        arbol.adicionarNodo(new Celular("1WE", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "yo");
//        arbol.adicionarNodo(new Celular("1BE", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "1WE");
//        arbol.adicionarNodo(new Celular("2WE", null, null, null, null, 0, (byte) 0), "1WE");
//        arbol.adicionarNodo(new Celular("2WE", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "1WE");
//        arbol.adicionarNodo(new Celular("2WE", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "1WE");
//        arbol.adicionarNodo(new Celular("1AV", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "1BE");
//        arbol.adicionarNodo(new Celular("1AV", null, null, null, null, 0, (byte) 0), "1BE");
//        arbol.adicionarNodo(new Celular("1AV", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "1BE");
//        arbol.adicionarNodo(new Celular("1AV", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "1BE");
//        arbol.adicionarNodo(new Celular("3WE", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "2WE");
//        arbol.adicionarNodo(new Celular("3WE", null, null, null, null, 0, (byte) 0), "2WE");
//        arbol.adicionarNodo(new Celular("3WE", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "2WE");
//        arbol.adicionarNodo(new Celular("3WE", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "2WE");
//        arbol.adicionarNodo(new Celular("3WE", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "2WE");
//        arbol.adicionarNodo(new Celular("06E", null, null, null, null, 0, (byte) 0), "1AV");
//        arbol.adicionarNodo(new Celular("06E", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "1AV");
//        arbol.adicionarNodo(new Celular("06E", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "1AV");
//        arbol.adicionarNodo(new Celular("06E", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "1AV");
//        arbol.adicionarNodo(new Celular("56E", null, null, null, null, 0, (byte) 0), "3WE");
//        arbol.adicionarNodo(new Celular("9RR", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "56E");
//        arbol.adicionarNodo(new Celular("9RR", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "56E");

//        System.out.println("El nivel de: " + arbol.retornalNivel(arbol.getRaiz(), "1BE", 0));
//        List<NodoN> lista = arbol.imprimirNodosxNivel(0);
//        for(NodoN nodo:lista){
//            System.out.println(nodo.getDato() + "\n");
//        }
        
    }
}
