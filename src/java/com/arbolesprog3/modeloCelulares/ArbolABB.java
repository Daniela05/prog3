/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.utilidades.JsfUtil;
import com.arbolesprog3.validador.CelularValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniela
 */
public class ArbolABB implements Serializable {

    private NodoABB raiz;
    private int cantidadNodos;
    String[] niveles;
    int altura;

    public ArbolABB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public String[] getNiveles() {
        return niveles;
    }

    public void setNiveles(String[] niveles) {
        this.niveles = niveles;
    }

    public double sumarPrecios() {

        return sumarPostOrden(raiz);
    }

    private double sumarPostOrden(NodoABB reco) {
        double suma = 0;
        if (reco != null) {

            suma += sumarPostOrden(reco.getIzquierda());
            suma += sumarPostOrden(reco.getDerecha());
            suma += reco.getDato().getPrecio();

        }
        return suma;
    }

    public int sumarInOrdenRecursivo(NodoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidad();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;

    }
    
    public double calcularPromedioPrecios() {
        return sumarPrecios() / (double) cantidadNodos;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato) throws CelularExcepcion {

        CelularValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo, raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) == 0) {
            throw new CelularExcepcion("Ya existe un celular con el imei "
                    + nuevo.getDato().getImei());
        }else if(nuevo.getDato().getCodigo()==pivote.getDato().getCodigo()){
            throw new CelularExcepcion("Ya existe un celuar con el codigo" + nuevo.getDato().getCodigo());
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().
                getImei()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }

    public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerInOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    public void recorrerPreOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listado);
            recorrerPreOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPostOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listado);
            recorrerPostOrden(reco.getDerecha(), listado);
            listado.add(reco.getDato());
        }
    }

    public boolean esVacio() {
        return raiz == null;
    }

    public int contarCelularesxLetra(String letra) {
        return contarCelularesXMarca(raiz, letra);
    }

    private int contarCelularesXMarca(NodoABB pivote, String letra) {
        int cont = 0;
        if (pivote != null) {
            if (pivote.getDato().getMarca().getDescripcion().toUpperCase().contains(letra.toUpperCase())) {
                cont++;
            }
            cont += contarCelularesXMarca(pivote.getIzquierda(), letra);
            cont += contarCelularesXMarca(pivote.getDerecha(), letra);
            return cont;
        }
        return cont;
    }

    private NodoABB buscarMin(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public int cantidadNodosHoja() {

        return cantidadNodosHoja(raiz);
    }

    private int cantidadNodosHoja(NodoABB pivote) {
        int cant = 0;
        if (pivote != null) {
            if (pivote.getIzquierda() == null && pivote.getDerecha() == null) {
                cant++;
            }
            cant += cantidadNodosHoja(pivote.getIzquierda());
            cant += cantidadNodosHoja(pivote.getDerecha());
            return cant;
        }
        return 0;
    }

    public int retornarAltura() {
        altura = 0;
        return retornarAltura(raiz, 1);
    }

    private int retornarAltura(NodoABB pivote, int nivel) {

        if (pivote != null) {
            retornarAltura(pivote.getIzquierda(), nivel + 1);
            if (nivel > altura) {
                altura = nivel;
                return altura;
            }
            retornarAltura(pivote.getDerecha(), nivel + 1);
            return altura;
        }
        return 0;
    }

    public Celular menorValor() {
        NodoABB pivote = raiz;
        if (raiz != null) {

            while (pivote.getIzquierda() != null) {
                pivote = pivote.getIzquierda();
            }
        }
        return pivote.getDato();
    }

    public Celular mayorValor() {
        NodoABB pivote = raiz;
        if (raiz != null) {
            while (pivote.getDerecha() != null) {
                pivote = pivote.getDerecha();
            }
        }
        return pivote.getDato();
    }

    public void borrarMenor() {

        if (raiz != null) {
            if (raiz.getIzquierda() == null) {
                raiz = raiz.getDerecha();
            } else {
                NodoABB anterior = raiz;
                NodoABB reco = raiz.getIzquierda();
                while (reco.getIzquierda() != null) {
                    anterior = reco;
                    reco = reco.getIzquierda();
                }

                anterior.setIzquierda(reco.getDerecha());
                cantidadNodos--;
            }
        }

    }

    public void borrarMayor() {

        if (raiz != null) {
            if (raiz.getDerecha() == null) {
                raiz = raiz.getIzquierda();
            } else {
                NodoABB anterior = raiz;
                NodoABB reco = raiz.getDerecha();
                while (reco.getDerecha() != null) {
                    anterior = reco;
                    reco = reco.getDerecha();
                }

                anterior.setDerecha(reco.getIzquierda());
                cantidadNodos--;
            }
        }

    }

    public NodoABB borrarDato(String imei) {

        NodoABB nodoB = borrarDato(raiz, imei);
        this.setRaiz(nodoB);
        return nodoB;

    }

    private NodoABB borrarDato(NodoABB pivote, String imei) {
        if (pivote == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = pivote.getDato().getImei().compareTo(imei);
        if (compara > 0) {
            pivote.setIzquierda(borrarDato(pivote.getIzquierda(), imei));
        } else if (compara < 0) {
            pivote.setDerecha(borrarDato(pivote.getDerecha(), imei));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {

                NodoABB cambiar = buscarMin(pivote.getDerecha());
                Celular aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarDato(pivote.getDerecha(), imei));
            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
                cantidadNodos--;
            }
        }
        return pivote;
    }

    public boolean buscarCelular(String imeiBuscar) {
        return (buscarCelular(this.raiz, imeiBuscar));

    }

    private boolean buscarCelular(NodoABB pivote, String imeiBuscar) {

        if (pivote == null) {

            return false;

        }

        int compara = ((Comparable) pivote.getDato().getImei()).compareTo(imeiBuscar);
        if (compara > 0) {
            return (buscarCelular(pivote.getIzquierda(), imeiBuscar));
        } else if (compara < 0) {
            return (buscarCelular(pivote.getDerecha(), imeiBuscar));
        } else {

            return true;
        }
    }

    protected boolean esHoja(NodoABB pivote) {
        return (pivote != null && pivote.getIzquierda() == null && pivote.getDerecha() == null);
    }

    public ArrayList listarHojas() {
        ArrayList hojas = new ArrayList();
        listarHojas(raiz, hojas);
        return (hojas);
    }

    private void listarHojas(NodoABB pivote, ArrayList hojas) {
        if (pivote != null) {
            if (this.esHoja(pivote)) {
                hojas.add(pivote.getDato());
            }
            listarHojas(pivote.getIzquierda(), hojas);
            listarHojas(pivote.getDerecha(), hojas);
        }

    }

    public Celular buscarPadre(String imei) {
        if (imei.equals("") || raiz == null) {
            return null;
        }
        NodoABB padre = buscarPadre(raiz, imei);
        if (padre == null) {
            return null;
        }
        return padre.getDato();
    }

    private NodoABB buscarPadre(NodoABB pivote, String imei) {
        if (pivote == null) {
            return null;
        }
        if ((pivote.getIzquierda() != null && pivote.getIzquierda().getDato().getImei().compareTo(imei) == 0)
                || (pivote.getDerecha() != null && pivote.getDerecha().getDato().getImei().compareTo(imei) == 0)) {
            return (pivote);
        }
        NodoABB nodo = buscarPadre(pivote.getIzquierda(), imei);
        if (nodo == null) {
            return (buscarPadre(pivote.getDerecha(), imei));
        } else {
            return nodo;
        }
    }

    public ArrayList impNiveles() {
        ArrayList niveles = new ArrayList();
        impNiveles(raiz, 1, niveles);
        return niveles;
    }

    private void impNiveles(NodoABB pivote, int nivel, ArrayList niveles) {
        if (pivote != null) {
            impNiveles(pivote.getIzquierda(), nivel + 1, niveles);
            niveles.add(pivote.getDato() + " Nivel: (" + nivel + ") ");
            impNiveles(pivote.getDerecha(), nivel + 1, niveles);
        }
    }

    public ArrayList imprimirNivel() {
        niveles = new String[altura + 1];
        ArrayList nivel = new ArrayList();
        imprimirNivel(raiz, 0);
        for (int i = 0; i < niveles.length; i++) {
            nivel.add(niveles[i] + " ");

        }
        return nivel;
    }

    public void imprimirNivel(NodoABB pivote, int nivel2) {
        if (pivote != null) {
            niveles[nivel2] = pivote.getDato() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
            imprimirNivel(pivote.getDerecha(), nivel2 + 1);
            imprimirNivel(pivote.getIzquierda(), nivel2 + 1);
        }
    }

    public void podar() {
        podar(raiz);
    }

    private void podar(NodoABB pivote) {
        if (pivote == null) {
            return;
        }
        if (this.esHoja(pivote.getIzquierda())) {
            pivote.setIzquierda(null);
            cantidadNodos--;
        }
        if (this.esHoja(pivote.getDerecha())) {
            pivote.setDerecha(null);
            cantidadNodos--;
        }
        podar(pivote.getIzquierda());
        podar(pivote.getDerecha());
    }

    public static void main(String[] args) {

        ArbolABB arbol = new ArbolABB();
//
//        try {
//            arbol.adicionarNodo(new Celular("1we", "23324234", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("1be", "123123", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("1av", "123213", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("06e", "123123", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("2we", "12321321", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("3we", "12323", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("56e", "213123", null, null, null, 0, (byte) 2));
//            arbol.adicionarNodo(new Celular("9rr", "213213", null, null, null, 0, (byte) 2));
//        } catch (CelularExcepcion ex) {
//            Logger.getLogger(ArbolABB.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        System.out.println(arbol.imprimirNivel());
        
    }
}
