/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniela
 */
public class NodoN implements Serializable{
    
    Celular dato;
    List<NodoN> hijos;

    public NodoN() {
    }

    public NodoN(Celular dato) {
        this.dato = dato;
        hijos = new ArrayList<>();
    }

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public List<NodoN> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoN> hijos) {
        this.hijos = hijos;
    }

    
    
    
    
}
