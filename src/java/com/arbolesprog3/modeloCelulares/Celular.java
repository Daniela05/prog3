/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class Celular implements Serializable {

    private int codigo;
    private String imei;
    private String numeroLinea;
    private Marca marca;
    private Operador operador;
    private String color;
    private double precio;
    private byte cantidad;

    public Celular() {
        marca = new Marca();
        operador = new Operador();
    }

    public Celular(int codigo, String imei, String numeroLinea, Marca marca, Operador operador, String color, double precio, byte cantidad) {
        this.codigo = codigo;
        this.imei = imei;
        this.numeroLinea = numeroLinea;
        this.marca = marca;
        this.operador = operador;
        this.color = color;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getNumeroLinea() {
        return numeroLinea;
    }

    public void setNumeroLinea(String numeroLinea) {
        this.numeroLinea = numeroLinea;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Operador getOperador() {
        return operador;
    }

    public void setOperador(Operador operador) {
        this.operador = operador;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public byte getCantidad() {
        return cantidad;
    }

    public void setCantidad(byte cantidad) {
        this.cantidad = cantidad;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return imei;
    }

}
