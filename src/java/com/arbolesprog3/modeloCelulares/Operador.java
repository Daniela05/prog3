/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloCelulares;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class Operador implements Serializable{
    private String nombre;
    private String direccion;
    private String cobertura;

    public Operador() {
    }
    
    

    public Operador(String nombre, String direccion, String cobertura) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.cobertura = cobertura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCobertura() {
        return cobertura;
    }

    public void setCobertura(String cobertura) {
        this.cobertura = cobertura;
    }

    @Override
    public String toString() {
        return "Operador{" + "nombre=" + nombre + ", direccion=" + direccion + ", cobertura=" + cobertura + '}';
    }
    
    
    
}
