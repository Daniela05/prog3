/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.excepcion;

/**
 *
 * @author daniela
 */
public class BarcoExcepcion extends Exception {

    
    public BarcoExcepcion() {
    }

   public BarcoExcepcion(String message) {
        super(message);
    }
   
   public BarcoExcepcion(String message, Throwable cause) {
        super(message, cause);
    }
   
   public BarcoExcepcion(Throwable cause) {
        super(cause);
    }
   
}
