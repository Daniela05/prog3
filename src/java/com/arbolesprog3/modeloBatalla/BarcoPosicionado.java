/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniela
 */
public class BarcoPosicionado implements Serializable {

    private TipoBarco tipobarco;
    private List<Coordenada> coordenadas;
    private byte nroImpactos;
    private Estado estado;
    private int identificador;

    public BarcoPosicionado() {

    }

    public BarcoPosicionado(TipoBarco tipobarco) {
        this.tipobarco = tipobarco;
        coordenadas = new ArrayList<>();
        estado = new Estado((byte) 0, "Activo");
    }

    public TipoBarco getTipobarco() {
        return tipobarco;
    }

    public void setTipobarco(TipoBarco tipobarco) {
        this.tipobarco = tipobarco;
    }

    public List<Coordenada> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(List<Coordenada> coordenadas) {
        this.coordenadas = coordenadas;
    }

    public byte getNroImpactos() {
        return nroImpactos;
    }

    public void setNroImpactos(byte nroImpactos) {
        this.nroImpactos = nroImpactos;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public boolean validarCoordenada(int fila, int columna) {
        for (Coordenada coord : coordenadas) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {

                return true;

            }
        }
        return false;
    }

    public boolean validarCoordenadaJuego(int fila, int columna) {
        for (Coordenada coord : coordenadas) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                if (!coord.isEstado()) {
                    return true;
                }

            }
        }
        return false;
    }

    public boolean validarImpacto(int fila, int columna) {
        for (Coordenada coord : coordenadas) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                coord.setEstado(false);
                return true;
            }
        }
        return false;
    }

    public boolean verificarEstadoBarco() {

        nroImpactos = 0;
        for (Coordenada coord : coordenadas) {
            if (!coord.isEstado()) {
                nroImpactos++;
            }
            if (nroImpactos == tipobarco.getNroCasillas()) {
                estado = new Estado((byte) 2, "Destruido");
                return true;
            } else if (nroImpactos < tipobarco.getNroCasillas() && nroImpactos > 0) {
                estado = new Estado((byte) 1, "Tocado");
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return tipobarco + "" + identificador + "\n" + coordenadas;
    }

    public String mostrarCoordenadas() {
        String coordenada = "Coordenadas:";
        String[] aux = new String[coordenadas.size()];
        if (coordenadas != null) {
            for (Coordenada coor : coordenadas) {

            }
            return coordenadas.toString();
        }
        return "Coordendas vacias";
    }

}
