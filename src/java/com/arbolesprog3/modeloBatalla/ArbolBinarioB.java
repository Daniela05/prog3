/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;

import com.arbolesprog3.utilidades.JsfUtil;
import com.arbolesprog3.validador.TipoBarcoValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniela
 */
public class ArbolBinarioB implements Serializable {

    private NodoABB raiz;

    private int cantidadNodos;
//    private int cantidadNodosUsu;
//    private List<Usuario> listaUsuario = new ArrayList<>();

    public ArbolBinarioB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

//    public List<Usuario> getListaUsuario() {
//        return listaUsuario;
//    }
//
//    public void setListaUsuario(List<Usuario> listaUsuario) {
//        this.listaUsuario = listaUsuario;
//    }
    public void adicionarNodo(TipoBarco dato) throws BarcoExcepcion {

        TipoBarcoValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo, raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws BarcoExcepcion {
        if (nuevo.getDato().getNombre().compareTo(pivote.getDato().getNombre()) == 0
                || nuevo.getDato().getNroCasillas() == pivote.getDato().getNroCasillas()) {
            throw new BarcoExcepcion("Ya existe un tipo de barco con  el nombre: \t"
                    + nuevo.getDato().getNombre() + " \t ó con el mismo numero de casillas: \t" + nuevo.getDato().getNroCasillas());
        } else if (nuevo.getDato().getNroCasillas()< pivote.getDato().getNroCasillas()) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }

//    public String guardarUsuario(Usuario usuario, Rol rol) {
//        String agregar = "";
//        for (Usuario listaUsuarios : listaUsuario) {
//            if (usuario.getCorreo().equals(listaUsuarios.getCorreo())) {
//                agregar = "correo";
//            }
//            if (rol.getNombre().equals(listaUsuarios.getTipoRol().getNombre())) {
//                agregar = "rol";
//            }
//
//        }
//
//        if (agregar.length() == 0) {
//            rol = new Rol(rol.getCodigo(), rol.getNombre());
//            listaUsuario.add(new Usuario(usuario.getCorreo(), usuario.getPassword(), rol));
//        }
//        return agregar;
//    }
//    public List<Usuario> listarUsuarios() {
//        return listaUsuario;
//    }
    public List<TipoBarco> ListarTipoBarcos() {
        List<TipoBarco> listaTipoBarcos = new ArrayList<>();
        recorrerInOrden(raiz, listaTipoBarcos);
        return listaTipoBarcos;
    }

    private void recorrerInOrden(NodoABB reco, List<TipoBarco> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
    }

    public int sumarCantidadBarcos() {

        return sumarInOrdenRecursivo(raiz);

    }

    private int sumarPostOrden(NodoABB reco) {
        int sumarBarcos = 0;
        if (reco != null) {
            sumarBarcos += sumarPostOrden(reco.getIzquierda());
            sumarBarcos += sumarPostOrden(reco.getDerecha());
            sumarBarcos += reco.getDato().getCantidadJuego();
            return sumarBarcos;
        }
        return 0;
    }
    
    public int sumarInOrdenRecursivo(NodoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidadJuego();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;

    }

    private NodoABB buscarMin(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return pivote;
    }
    
    

//    public List<TipoBarco> buscarBarco(String nombre) {
//         List<TipoBarco> barcosEncontrados = new ArrayList<>();
//         barcosEncontrados.add(buscarBarco(raiz, nombre).getDato());
//        return barcosEncontrados;
//
//    }
//    private NodoABB  buscarBarco(NodoABB pivote, String nombre) {
//
//        if (pivote == null) {
//
//            return null;
//
//        }
//
//        int compara =  pivote.getDato().getNombre().toUpperCase().compareTo(nombre.toUpperCase());
//        if (compara > 0) {
//            return (buscarBarco(pivote.getIzquierda(), nombre));
//        } else if (compara < 0) {
//            return (buscarBarco(pivote.getDerecha(), nombre));
//        } else {
//
//            return pivote;
//        }
//    }
    public NodoABB borrarDato(TipoBarco borrar) {

        NodoABB nodoB = borrarDato(raiz, borrar);
        this.setRaiz(nodoB);
        return nodoB;
    }

    private NodoABB borrarDato(NodoABB pivote, TipoBarco borrar) {
        if (pivote == null) {
            return null;
        }
            
        
        if (pivote.getDato().getNroCasillas() > borrar.getNroCasillas()) {
            pivote.setIzquierda(borrarDato(pivote.getIzquierda(), borrar));
        } else if (pivote.getDato().getNroCasillas() < borrar.getNroCasillas()) {
            pivote.setDerecha(borrarDato(pivote.getDerecha(), borrar));

        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {

                NodoABB cambiar = buscarMin(pivote.getDerecha());
                TipoBarco aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarDato(pivote.getDerecha(), borrar));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
                cantidadNodos--;
            }
        }
        return pivote;
    }

    public int cantidadNodosHoja() {

        return cantidadNodosHoja(raiz);
    }

    private int cantidadNodosHoja(NodoABB pivote) {
        int cant = 0;
        if (pivote != null) {
            if (pivote.getIzquierda() == null && pivote.getDerecha() == null) {
                cant++;
            }
            cant += cantidadNodosHoja(pivote.getIzquierda());
            cant += cantidadNodosHoja(pivote.getDerecha());
            return cant;
        }
        return 0;
    }

    protected boolean esHoja(NodoABB pivote) {
        return (pivote != null && pivote.getIzquierda() == null && pivote.getDerecha() == null);
    }

    public ArrayList listarHojas() {
        ArrayList hojas = new ArrayList();
        listarHojas(raiz, hojas);
        return (hojas);
    }

    private void listarHojas(NodoABB pivote, ArrayList hojas) {
        if (pivote != null) {
            if (this.esHoja(pivote)) {
                hojas.add(pivote.getDato());
            }
            listarHojas(pivote.getIzquierda(), hojas);
            listarHojas(pivote.getDerecha(), hojas);
        }

    }

    public boolean buscarTipoBarco(String imeiBuscar) {
        return (buscarTipoBarco(raiz, imeiBuscar));

    }

    private boolean buscarTipoBarco(NodoABB pivote, String nombre) {

        if (pivote == null) {

            return false;

        }

        int compara = ((Comparable) pivote.getDato().getNombre()).compareTo(nombre);
        if (compara > 0) {
            return (buscarTipoBarco(pivote.getIzquierda(), nombre));
        } else if (compara < 0) {
            return (buscarTipoBarco(pivote.getDerecha(), nombre));
        } else {

            return true;
        }
    }

    public TipoBarco buscarPadre(String nombre) {
        if (nombre.equals("") || raiz == null) {
            return null;
        }
        NodoABB padre = buscarPadre(raiz, nombre);
        if (padre == null) {
            return null;
        }
        return padre.getDato();
    }

    private NodoABB buscarPadre(NodoABB pivote, String nombre) {
        if (pivote == null) {
            return null;
        }
        if ((pivote.getIzquierda() != null && pivote.getIzquierda().getDato().getNombre().compareTo(nombre) == 0)
                || (pivote.getDerecha() != null && pivote.getDerecha().getDato().getNombre().compareTo(nombre) == 0)) {
            return (pivote);
        }
        NodoABB nodo = buscarPadre(pivote.getIzquierda(), nombre);
        if (nodo == null) {
            return (buscarPadre(pivote.getDerecha(), nombre));
        } else {
            return nodo;
        }
    }

    public void podar() {
        podar(raiz);
    }

    private void podar(NodoABB pivote) {
        if (pivote == null) {
            return;
        }
        if (this.esHoja(pivote.getIzquierda())) {
            pivote.setIzquierda(null);
            cantidadNodos--;
        }
        if (this.esHoja(pivote.getDerecha())) {
            pivote.setDerecha(null);
            cantidadNodos--;
        }
        podar(pivote.getIzquierda());
        podar(pivote.getDerecha());
    }


}
