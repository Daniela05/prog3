/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class Usuario implements Serializable{
    
    private String correo;
    private String password;
    private Rol tipoRol;

    public Usuario() {
    }
    
    

    public Usuario(String correo, String password, Rol tipoRol) {
        this.correo = correo;
        this.password = password;
        this.tipoRol = tipoRol;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rol getTipoRol() {
        return tipoRol;
    }

    public void setTipoRol(Rol tipoRol) {
        this.tipoRol = tipoRol;
    }
    
    
}
