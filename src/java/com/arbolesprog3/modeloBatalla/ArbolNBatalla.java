/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author daniela
 */
public class ArbolNBatalla implements Serializable {

    NodoN raiz;
    int cantidadNodos;
    

    public ArbolNBatalla() {
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public boolean esHoja(NodoN nodo) {
        if (nodo.equals(raiz)) {
            return false;

        }

        return nodo.getHijos() == null;

    }

    public NodoN buscarNodoxNombre(String nombre) {
        if (raiz != null) {
            return buscarNodoxNombre(nombre, raiz);
        }
        return null;
    }

    private NodoN buscarNodoxNombre(String nombre, NodoN pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado;
                }
            }
        }
        return null;
    }

    public BarcoPosicionado buscarBarcoxNombre(String nombre) {
        if (raiz != null) {
            return buscarBarcoxNombre(nombre, raiz);
        }
        return null;
    }

    private BarcoPosicionado buscarBarcoxNombre(String nombre, NodoN pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote.getDato();
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado.getDato();
                }
            }
        }
        return null;
    }

    public NodoN buscarPadre(String nombre) {
        if (raiz.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return null;
        }
        return buscarPadre(nombre, raiz);
    }

    private NodoN buscarPadre(String nombre, NodoN pivote) {
        for (NodoN hijo : pivote.getHijos()) {
            if (hijo.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
                return pivote;
            } else {
                NodoN padreBuscado = buscarPadre(nombre, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    public void adicionarNodo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoN(dato);
        } else {
            NodoN padreEncontrado = buscarNodoxNombre(padre.getTipobarco().getNombre());
            if (padreEncontrado != null) {
                padreEncontrado.getHijos().add(new NodoN(dato));
            }

        }
        cantidadNodos++;
    }

    public void adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;
    }

    public boolean adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre, NodoN pivote) {
        // boolean adicionado=false;
        if (pivote.getDato().getIdentificador() == padre.getIdentificador()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public List<BarcoPosicionado> listarBarcosPosicionados() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcosPosicionados(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcosPosicionados(NodoN nodo, List<BarcoPosicionado> barcos) {

        for (NodoN agregar : nodo.getHijos()) {
            
                barcos.add(agregar.getDato());
            

            listarBarcosPosicionados(agregar, barcos);

        }
    }
    public List<BarcoPosicionado> listarBarcos() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcos(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcos(NodoN nodo, List<BarcoPosicionado> barcos) {

        nodo.getHijos().stream().map((agregar) -> {
            if (agregar.getDato().getCoordenadas().isEmpty()) {
                barcos.add(agregar.getDato());
            }
            return agregar;
        }).forEach((agregar) -> {
            listarBarcos(agregar, barcos);
        });
    }
//    
//    public Coordenada[] instanciarCoordenadas(Coordenada coordenada, String direccion){
//        return instanciarCoordenadas(coordenada, direccion, raiz.dato.getTipobarco());
//    }

    public void instanciarCoordenadas(BarcoPosicionado barco, String direccion, int fila, int columna) {
        if (barco.getCoordenadas().isEmpty()) {
            barco.setCoordenadas(new ArrayList<>());
        }
        if (direccion.equals("Vertical")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                barco.getCoordenadas().add(new Coordenada((byte) (columna), (byte) (fila + i)));

            }
        } else if (direccion.equals("Horizontal")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                barco.getCoordenadas().add(new Coordenada((byte) (columna + i), (byte) (fila)));

            }
        }
        
        

    }
}
