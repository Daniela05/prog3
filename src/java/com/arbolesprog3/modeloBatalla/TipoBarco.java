/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class TipoBarco implements Serializable{
    private String nombre;
    private byte nroCasillas;
    private byte cantidadJuego;

    public TipoBarco() {
    }

    public TipoBarco(String nombre, byte nroCasillas, byte cantidadJuego) {
        this.nombre = nombre;
        this.nroCasillas = nroCasillas;
        this.cantidadJuego = cantidadJuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte getNroCasillas() {
        return nroCasillas;
    }

    public void setNroCasillas(byte nroCasillas) {
        this.nroCasillas = nroCasillas;
    }

    public byte getCantidadJuego() {
        return cantidadJuego;
    }

    public void setCantidadJuego(byte cantidadJuego) {
        this.cantidadJuego = cantidadJuego;
    }

    

   

    @Override
    public String toString() {
        return nombre;
    }
    
    
}
