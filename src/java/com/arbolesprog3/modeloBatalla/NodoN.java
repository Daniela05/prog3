/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniela
 */
public class NodoN implements Serializable{
    BarcoPosicionado dato;
    List<NodoN> hijos;

    public NodoN() {
    }

    public NodoN(BarcoPosicionado dato) {
        this.dato = dato;
        hijos = new ArrayList<>();
    }

    public BarcoPosicionado getDato() {
        return dato;
    }

    public void setDato(BarcoPosicionado dato) {
        this.dato = dato;
    }

    public List<NodoN> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoN> hijos) {
        this.hijos = hijos;
    }
    
    
}
