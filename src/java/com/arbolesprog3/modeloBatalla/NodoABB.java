/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import java.io.Serializable;

/**
 *
 * @author daniela
 */
public class NodoABB implements Serializable {

    private TipoBarco dato;

    private NodoABB izquierda;
    private NodoABB derecha;

    public NodoABB(TipoBarco dato) {
        this.dato = dato;
    }

   

    public TipoBarco getDato() {
        return dato;
    }

    public void setDato(TipoBarco dato) {
        this.dato = dato;
    }

    public NodoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABB derecha) {
        this.derecha = derecha;
    }

   

}
