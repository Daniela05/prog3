/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.validador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modeloCelulares.Celular;

/**
 *
 * @author daniela
 */
public class CelularValidador {
  public static void validarDatos(Celular celular) throws CelularExcepcion
  {
      if(celular.getImei()==null || celular.getImei().equals("")
              || celular.getImei().startsWith(" "))
      {
          throw new CelularExcepcion("Debe diligenciar el imei");
      }
      if(celular.getNumeroLinea()==null || celular.getNumeroLinea().equals("")
              || celular.getNumeroLinea().startsWith(" "))
      {
          throw new CelularExcepcion("Debe diligenciar el número línea");
      }
  }   
}
