/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.validador;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modeloBatalla.TipoBarco;
import com.arbolesprog3.modeloBatalla.Usuario;

/**
 *
 * @author daniela
 */
public class TipoBarcoValidador {
    
     public static void validarDatos(TipoBarco barco) throws BarcoExcepcion
  {
      if(barco.getNombre()==null || barco.getNombre().equals("")
              || barco.getNombre().startsWith(" "))
      {
          throw new BarcoExcepcion("Debe diligenciar el nombre");
      }
      if(barco.getNroCasillas()<= 0)
              
      {
          throw new BarcoExcepcion("Debe seleccionar el numero de casillas");
      }
      
  } 
     
       public static void validarDatos(Usuario usuario) throws BarcoExcepcion
  {
      if(usuario.getCorreo()==null || usuario.getCorreo().equals("")
              || usuario.getCorreo().startsWith(" "))
      {
          throw new BarcoExcepcion("Debe diligenciar el nombre");
      }
     if(usuario.getPassword()==null || usuario.getPassword().equals("")
              || usuario.getPassword().startsWith(" "))
              
      {
          throw new BarcoExcepcion("Debe deligenciar la contraseña");
      }
      
  }
}
