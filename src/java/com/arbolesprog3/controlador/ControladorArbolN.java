/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modeloCelulares.ArbolN;
import com.arbolesprog3.modeloCelulares.Celular;
import com.arbolesprog3.modeloCelulares.Marca;
import com.arbolesprog3.modeloCelulares.NodoABB;
import com.arbolesprog3.modeloCelulares.NodoN;
import com.arbolesprog3.modeloCelulares.Operador;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author daniela
 */
@Named(value = "controladorArbolN")
@SessionScoped
public class ControladorArbolN implements Serializable {

    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private NodoN nodon;
    private ArbolN arbolN = new ArbolN();
    private ControladorABB contAbb=(ControladorABB) FacesUtils.
                     getManagedBean("controladorABB");

    private Celular padre;
    private String operador;
    private String imeiBorrar;
    private String marcaBorrar;

    private boolean verRegistrar = false;
    private boolean habilitarTabla = false;

    /**
     * Creates a new instance of ControladorArbolN
     */
    public ControladorArbolN() {
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public NodoN getNodon() {
        return nodon;
    }

    public void setNodon(NodoN nodon) {
        this.nodon = nodon;
    }

    public ArbolN getArbolN() {
        return arbolN;
    }

    public void setArbolN(ArbolN arbolN) {
        this.arbolN = arbolN;
    }

    public Celular getPadre() {
        return padre;
    }

    public void setPadre(Celular padre) {
        this.padre = padre;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public boolean isHabilitarTabla() {
        return habilitarTabla;
    }

    public void setHabilitarTabla(boolean habilitarTabla) {
        this.habilitarTabla = habilitarTabla;
    }

    public String getImeiBorrar() {
        return imeiBorrar;
    }

    public void setImeiBorrar(String imeiBorrar) {
        this.imeiBorrar = imeiBorrar;
    }

    public String getMarcaBorrar() {
        return marcaBorrar;
    }

    public void setMarcaBorrar(String marcaBorrar) {
        this.marcaBorrar = marcaBorrar;
    }

    @PostConstruct
    public void iniciar() {
        llenarMarcas();
        llenarOperadores();
        try {
            cargarArbol();
        } catch (CelularExcepcion ex) {
            Logger.getLogger(ControladorArbolN.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        arbolN.adicionarNodo(new Celular("yo", null, null, null, null, 0, (byte) 0), null);
        arbolN.adicionarNodo(new Celular("A", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "yo");
        arbolN.adicionarNodo(new Celular("A", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "yo");
        arbolN.adicionarNodo(new Celular("A", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "yo");
        arbolN.adicionarNodo(new Celular("B", null, null, null, null, 0, (byte) 0), "A");
        arbolN.adicionarNodo(new Celular("C", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "A");
        arbolN.adicionarNodo(new Celular("C", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "A");
        arbolN.adicionarNodo(new Celular("B", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "A");
        arbolN.adicionarNodo(new Celular("C", null, null, null, null, 0, (byte) 0), "A");
        arbolN.adicionarNodo(new Celular("C", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "A");
        arbolN.adicionarNodo(new Celular("D", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "B");
        arbolN.adicionarNodo(new Celular("E", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "B");
        arbolN.adicionarNodo(new Celular("F", null, null, null, null, 0, (byte) 0), "C");
        arbolN.adicionarNodo(new Celular("F", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "C");
        arbolN.adicionarNodo(new Celular("E", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "B");
        arbolN.adicionarNodo(new Celular("F", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "C");
        arbolN.adicionarNodo(new Celular("F", null, null, null, null, 0, (byte) 0), "C");
        arbolN.adicionarNodo(new Celular("G", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "E");
        arbolN.adicionarNodo(new Celular("H", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "F");
        arbolN.adicionarNodo(new Celular("H", "3108707617", new Marca((short) 3, "Samsung"), new Operador("Movistar", "Medellin", "4G"), "Gris", 767000, (byte) 2), "F");
        arbolN.adicionarNodo(new Celular("H", null, null, null, null, 0, (byte) 0), "F");
        arbolN.adicionarNodo(new Celular("I", "3106585495", new Marca((short) 1, "LG"), new Operador("Claro", "Barii", "3G"), "Blanco", 507000, (byte) 4), "H");
        arbolN.adicionarNodo(new Celular("J", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 3), "I");
        arbolN.adicionarNodo(new Celular("J", "3107696506", new Marca((short) 2, "Sony"), new Operador("Tigo", "Barii00", "4G"), "Rosa", 707000, (byte) 4), "I");
         */
        pintarArbol();
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

//    public void cargarArbol() {
//
//        //Algoritmo
//        arbolN.adicionarNodo(new Celular("MiTienda", null, new Marca((byte) 2, "LG"), new Operador("Claro", "Medellin", "4G LTE"), null, 0, (byte) 0), null);
//        ControladorABB contAbb = (ControladorABB) FacesUtils.getManagedBean("controladorABB");
//
//        System.out.println("Cantidad: " + contAbb.getArbol().
//                getCantidadNodos());
//        int cont = 0;
//        List<NodoN> hijos = new ArrayList<>();
//        for (Celular celu : contAbb.getArbol().recorrerPreOrden()) {
//            List<NodoN> padres = arbolN.imprimirNodosxNivel(cont);
//            if (celu != null) {
//                NodoABB pivote = contAbb.getArbol().getRaiz();
//                NodoABB aux = pivote;
//
//                for (int i = 0; i < celu.getCantidad(); i++) {
//                    if (celu.getImei().equals(pivote.getDato().getImei())) {
//                        arbolN.adicionarNodo(celu, padres.get(0).getDato().getImei());
//
//                    } else if (celu.getImei().equals(pivote.getIzquierda().getDato().getImei())
//                            || celu.getImei().equals(aux.getDerecha().getDato().getImei())) {
//                        arbolN.adicionarNodo(celu, pivote.getDato().getImei());
//
//                    } else if (celu.getImei().equals(pivote.getIzquierda().getIzquierda().getDato().getImei())
//                            || celu.getImei().equals(aux.getDerecha().getDerecha().getDato().getImei())) {
//                        if (contAbb.getArbol().buscarPadre(celu.getImei()).getImei().equals(pivote.getIzquierda().getDato().getImei())) {
//                            arbolN.adicionarNodo(celu, pivote.getIzquierda().getDato().getImei());
//                        } else {
//                            arbolN.adicionarNodo(celu, pivote.getDerecha().getDato().getImei());
//                        }
//
//                    } else if (celu.getImei().equals(pivote.getIzquierda().getIzquierda().getIzquierda().getDato().getImei())
//                            || celu.getImei().equals(aux.getDerecha().getDerecha().getDerecha().getDato().getImei())) {
//                        if (contAbb.getArbol().buscarPadre(celu.getImei()).getImei().equals(pivote.getIzquierda().getIzquierda().getDato().getImei())) {
//                            arbolN.adicionarNodo(celu, pivote.getIzquierda().getIzquierda().getDato().getImei());
//                        } else {
//                            arbolN.adicionarNodo(celu, pivote.getDerecha().getDerecha().getDato().getImei());
//                        }
//                    } else if (celu.getImei().equals(aux.getDerecha().getDerecha().getDerecha().getDerecha().getDato().getImei())) {
//
//                        if (contAbb.getArbol().buscarPadre(celu.getImei()).getImei().equals(pivote.getIzquierda().getIzquierda().getIzquierda().getDato().getImei())) {
//                            arbolN.adicionarNodo(celu, pivote.getIzquierda().getIzquierda().getIzquierda().getDato().getImei());
//                        } else {
//                            arbolN.adicionarNodo(celu, pivote.getDerecha().getDerecha().getDerecha().getDato().getImei());
//                        }
//                    }
//                }
//
//            }
//        }
//
//    }
    public void cargarArbol() throws CelularExcepcion {

        //Algoritmo
        arbolN.adicionarNodo(new Celular(0, "Mi Tienda", null, null, null, null, 0, (byte) 0), null);
        

        List<Celular> padres = new ArrayList<Celular>();
        padres.add(arbolN.getRaiz().getDato());
        adicionarPreOrden(contAbb.getArbol().getRaiz(), padres, 0);

    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void guardarCelular() {

        arbolN.adicionarNodo(celular, padre);
        celular = new Celular();
        verRegistrar = false;
        pintarArbol();
    }

    public void cancelarGuardado() {
        verRegistrar = false;

    }

    public List<Celular> listarCelularesxOperador() {
        List<Celular> listaCelulares = new ArrayList<>();
        if (arbolN.getRaiz().getDato().getOperador().getNombre().compareTo(this.operador) == 0) {
            listaCelulares.add(arbolN.getRaiz().getDato());
        }
        listarCelularesxOperador(arbolN.getRaiz(), listaCelulares, this.operador);
        return listaCelulares;
    }

    private void listarCelularesxOperador(NodoN nodo, List<Celular> celulares, String operador) {
        for (NodoN agregar : nodo.getHijos()) {
            if (agregar.getDato().getOperador().getNombre().compareTo(operador) == 0) {
                celulares.add(agregar.getDato());
            }
            listarCelularesxOperador(agregar, celulares, operador);
        }

    }

    public void verHabilitarTabla() {
        habilitarTabla = true;

    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);

        pintarArbol(arbolN.getRaiz(), model, null, 30, 0);
    }

    private void pintarArbol(NodoN reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));
            }

            model.addElement(elementHijo);
            for (int i = 0; i < reco.getHijos().size(); i++) {
                pintarArbol(reco.getHijos().get(i), model, elementHijo,x - reco.getHijos().size() - 3, y + 5);
                x += 10;

            }

        }
    }

//    private EndPoint createEndPoint(EndPointAnchor anchor) {
//        DotEndPoint endPoint = new DotEndPoint(anchor);
//        endPoint.setStyle("{fillStyle:'#404a4e'}");
//        endPoint.setHoverStyle("{fillStyle:'#20282b'}");
//
//        return endPoint;
//    }

    public DiagramModel getModel() {
        return model;
    }

    public void borrarDato() {
        arbolN.eliminarNodo(getImeiBorrar());
        JsfUtil.addSuccessMessage("Se ha borrado el celular con exito");
        pintarArbol();
    }

    public void eliminarNodoXMarca() {
        arbolN.eliminarNodoXMarca(getMarcaBorrar());

        pintarArbol();
    }

     private void adicionarPreOrden(NodoABB reco, List<Celular> padres, int contizq) throws CelularExcepcion{
        if (reco != null) {
            List<Celular> padresNuevos= new ArrayList<>();
            int contPapas=0;
            for(byte i=0;i< reco.getDato().getCantidad();i++)
            {    
                Celular nuevoCelular = new Celular(reco.getDato().getCodigo(),reco.getDato().getImei(), reco.getDato().getNumeroLinea(), reco.getDato().getMarca(),
                         reco.getDato().getOperador(), reco.getDato().getColor(), reco.getDato().getPrecio(), reco.getDato().getCantidad());
                      
                nuevoCelular.setCodigo(++contizq);
                if(contPapas>=padres.size())
                {
                    contPapas=0;
                }
                arbolN.adicionarNodoxCodigo(nuevoCelular, padres.get(contPapas));
                padresNuevos.add(nuevoCelular);
                contPapas++;
            }
            adicionarPreOrden(reco.getIzquierda(),padresNuevos, contizq);
            contizq=contizq+contAbb.getArbol().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrden(reco.getDerecha(),padresNuevos, contizq);
            
        
        }
    }
}
