/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.controladorBatalla.ControladorJuego;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author daniela
 */
@Named(value = "login")
@ApplicationScoped
public class Login {
 private String usuarioAdmin;
    private String contrasenaAdmin;
    private String usuarioJug;
    private String contrasenaJug;
    private String rol;
    ControladorJuego controlb = (ControladorJuego) FacesUtils.getManagedBean("controladorJuego");

    /**
     * Creates a new instance of Login
     */
    public Login() {
    }

    public String getUsuarioAdmin() {
        return usuarioAdmin;
    }

    public void setUsuarioAdmin(String usuarioAdmin) {
        this.usuarioAdmin = usuarioAdmin;
    }

    public String getContrasenaAdmin() {
        return contrasenaAdmin;
    }

    public void setContrasenaAdmin(String contrasenaAdmin) {
        this.contrasenaAdmin = contrasenaAdmin;
    }

    public String getUsuarioJug() {
        return usuarioJug;
    }

    public void setUsuarioJug(String usuarioJug) {
        this.usuarioJug = usuarioJug;
    }

    public String getContrasenaJug() {
        return contrasenaJug;
    }

    public void setContrasenaJug(String contrasenaJug) {
        this.contrasenaJug = contrasenaJug;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String ingresarArbolCelulares() {

        return "irCelulares";
    }

    public String regresarIndex() {

        return "regresar";
    }

    public String ingresarBatalla() {
        return "irBatalla";
    }

    public String ingresarLoginAdministrador() {
        return "irLoginAdmin";
    }
    
    public String ingresarArbolAvl() {
        return "irAvl";
    }

    public String ingresarMenuAdministrador() {
        if (usuarioAdmin.compareTo(controlb.getAdministrador().getCorreo()) == 0 && contrasenaAdmin.compareTo(controlb.getAdministrador().getPassword()) == 0) {
            return "irMenuAdministrador";
        } else if (usuarioAdmin.compareTo(controlb.getAdministrador().getCorreo()) != 0) {
            JsfUtil.addErrorMessage("Usuario Incorrecto");
            return null;
        } else if (contrasenaAdmin.compareTo(controlb.getAdministrador().getPassword()) != 0) {
            JsfUtil.addErrorMessage("Contraseña Incorrecta");
            return null;
        }
        JsfUtil.addErrorMessage("Datos Incorrectos");
        return null;
    }

    public String ingresarArbolNCelulares() {
        return "irArbolN";
    }
    
     public String ingresarJuegoBatalla1() {
        return "irAJuegoBatalla1";
    }
     
      public String ingresarJuegoBatalla2() {
        return "irAJuegoBatalla2";
    }

    public String ingresarMenuJugador() {
        if (usuarioJug.compareTo(controlb.getJugador1().getCorreo()) == 0 && contrasenaJug.compareTo(controlb.getJugador1().getPassword()) == 0
                && controlb.getJugador1().getTipoRol().getNombre().compareTo(rol) == 0) {
            return "irMenuJugador1";
        } else if (usuarioJug.compareTo(controlb.getJugador2().getCorreo()) == 0 && contrasenaJug.compareTo(controlb.getJugador2().getPassword()) == 0
                && controlb.getJugador2().getTipoRol().getNombre().compareTo(rol) == 0) {
            return "irMenuJugador2";

        } else if (usuarioJug.compareTo(controlb.getJugador1().getCorreo()) != 0 || usuarioJug.compareTo(controlb.getJugador2().getCorreo()) != 0) {
            JsfUtil.addErrorMessage("Usuario Incorrecto");
            return null;
        } else if (contrasenaJug.compareTo(controlb.getJugador2().getPassword()) != 0
                || contrasenaJug.compareTo(controlb.getJugador2().getPassword()) != 0) {
            JsfUtil.addErrorMessage("Contraseña Incorrecta");
            return null;
        }
        JsfUtil.addErrorMessage("Datos Incorrectos");
        return null;
    }

    public String ingresarLoginJugador() {
        return "irLoginJugador";
    }
    /**
     * Creates a new instance of Login
     */
//    public Login() {
//    }
    
}
