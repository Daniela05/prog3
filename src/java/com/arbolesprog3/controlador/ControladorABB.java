/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modeloCelulares.ArbolABB;
import com.arbolesprog3.modeloCelulares.Celular;
import com.arbolesprog3.modeloCelulares.Marca;
import com.arbolesprog3.modeloCelulares.NodoABB;
import com.arbolesprog3.modeloCelulares.Operador;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private boolean verRegistrar = false;
    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private NodoABB nodo;
    private ArbolABB arbol = new ArbolABB();
    private boolean habilitarTabla = false;
    private boolean habilitarTablaPre = false;
    private boolean habilitarTablaIn = false;
    private boolean habilitarTablaPos = false;
    private boolean impNivel = false;

    private String operador;
    private String letra;
    private String imeiBorrar;
    private String imeiBuscar;
    private String padre;
    private boolean contarLetra = false;
    private boolean habilitarBuscar = false;
    private boolean habilitaAltura = false;
    private boolean habilitarTablaHojas = false;
    private boolean habilitaPadre = false;

    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public NodoABB getNodo() {
        return nodo;
    }

    public void setNodo(NodoABB nodo) {
        this.nodo = nodo;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public ArbolABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABB arbol) {
        this.arbol = arbol;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public boolean isContarLetra() {
        return contarLetra;
    }

    public void setContarLetra(boolean contarLetra) {
        this.contarLetra = contarLetra;
    }

    public String getImeiBorrar() {
        return imeiBorrar;
    }

    public void setImeiBorrar(String imeiBorrar) {
        this.imeiBorrar = imeiBorrar;
    }

    public String getImeiBuscar() {
        return imeiBuscar;
    }

    public void setImeiBuscar(String imeiBuscar) {
        this.imeiBuscar = imeiBuscar;
    }

    public boolean isHabilitarBuscar() {
        return habilitarBuscar;
    }

    public void setHabilitarBuscar(boolean habilitarBuscar) {
        this.habilitarBuscar = habilitarBuscar;
    }

    public boolean isHabilitaAltura() {
        return habilitaAltura;
    }

    public void setHabilitaAltura(boolean habilitaAltura) {
        this.habilitaAltura = habilitaAltura;
    }

    public boolean isHabilitarTablaHojas() {
        return habilitarTablaHojas;
    }

    public void setHabilitarTablaHojas(boolean habilitarTablaHojas) {
        this.habilitarTablaHojas = habilitarTablaHojas;
    }

    public String getPadre() {
        return padre;
    }

    public void setPadre(String padre) {
        this.padre = padre;
    }

    public boolean isHabilitaPadre() {
        return habilitaPadre;
    }

    public void setHabilitaPadre(boolean habilitaPadre) {
        this.habilitaPadre = habilitaPadre;
    }

    public boolean isHabilitarTablaPre() {
        return habilitarTablaPre;
    }

    public void setHabilitarTablaPre(boolean habilitarTablaPre) {
        this.habilitarTablaPre = habilitarTablaPre;
    }

    public boolean isHabilitarTablaIn() {
        return habilitarTablaIn;
    }

    public void setHabilitarTablaIn(boolean habilitarTablaIn) {
        this.habilitarTablaIn = habilitarTablaIn;
    }

    public boolean isHabilitarTablaPos() {
        return habilitarTablaPos;
    }

    public void setHabilitarTablaPos(boolean habilitarTablaPos) {
        this.habilitarTablaPos = habilitarTablaPos;
    }

    public boolean isImpNivel() {
        return impNivel;
    }

    public void setImpNivel(boolean impNivel) {
        this.impNivel = impNivel;
    }

    @PostConstruct
    private void iniciar() {
        llenarMarcas();
        llenarOperadores();
        try {
            arbol.adicionarNodo(new Celular(1,"1we244", "3215489385", marcas.get(0), operadores.get(0), "negro", 250000,(byte)2));
            arbol.adicionarNodo(new Celular(2,"2we244", "3105474386", marcas.get(1), operadores.get(1), "blanco", 234900,(byte)3));
            arbol.adicionarNodo(new Celular(3,"1be244", "3115874387", marcas.get(2), operadores.get(0), "rojo", 423000,(byte)1));
            arbol.adicionarNodo(new Celular(4,"1av244", "3455474364", marcas.get(2), operadores.get(1), "azul", 500000,(byte)4));
            arbol.adicionarNodo(new Celular(5,"3we244", "3215474384", marcas.get(0), operadores.get(1), "dorado", 475000,(byte)5));
            arbol.adicionarNodo(new Celular(6,"06e244", "3215474384", marcas.get(1), operadores.get(0), "verde", 623900,(byte)4));
            arbol.adicionarNodo(new Celular(7,"56e244", "3215474384", marcas.get(0), operadores.get(1), "lila", 435800,(byte)1));
            arbol.adicionarNodo(new Celular(8,"9rrt44", "3215474384", marcas.get(1), operadores.get(0), "gris", 346700,(byte)2));
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

        pintarArbol();

    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public boolean isHabilitarTabla() {
        return habilitarTabla;
    }

    public void setHabilitarTabla(boolean habilitarTabla) {
        this.habilitarTabla = habilitarTabla;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void habilitarAltura() {
        habilitaAltura = true;

    }

    public void habilitarPadre() {
        habilitaPadre = true;

    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
    }

    private void pintarArbol(NodoABB reco, DefaultDiagramModel model,
            Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 8);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 8);
        }

    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

    public void guardarCelular() {
        try {
            arbol.adicionarNodo(celular);
            celular = new Celular();
            verRegistrar = false;
            pintarArbol();
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public List<Celular> recorrerPreOrdenOperador() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrdenOperador(arbol.getRaiz(), listaCelulares);
        habilitarTabla = true;
        return listaCelulares;

    }

    private void recorrerPreOrdenOperador(NodoABB reco, List<Celular> listado) {
        if (reco != null) {
            if (reco.getDato().getOperador().getNombre().equals(this.operador)) {
                listado.add(reco.getDato());
            }

            recorrerPreOrdenOperador(reco.getIzquierda(), listado);
            recorrerPreOrdenOperador(reco.getDerecha(), listado);
        }
    }

    public void verHabilitarTabla() {
        habilitarTabla = true;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
    }

    public void verPreorden() {
        habilitarTablaPre = true;
        habilitarTablaIn = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verInOrden() {
        habilitarTablaIn = true;
        habilitarTablaPre = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verPosorden() {
        habilitarTablaPos = true;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verHojas() {
        habilitarTablaHojas = true;
        habilitarTablaPos = false;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTabla = false;
    }

    public void verNiveles() {
        impNivel = true;
    }

    public void cancelarGuardado() {
        verRegistrar = false;

    }

    public void habilitarBusqueda() {
        habilitarBuscar = true;
    }

    public void habilitarContar() {
        contarLetra = true;
    }

    public int contarCelularesxLetra() {
        return arbol.contarCelularesxLetra(letra);
    }

    public void borrarMenor() {
        arbol.borrarMenor();
        pintarArbol();
    }

    public void borrarMayor() {
        arbol.borrarMayor();
        pintarArbol();
    }

    public void borrarDato() {
        arbol.borrarDato(getImeiBorrar());
        JsfUtil.addSuccessMessage("Se ha borrado el celular con exito");
        pintarArbol();
    }

    public void podar() {
        arbol.podar();
        pintarArbol();
    }

    public String buscarCelular() {
        if (arbol.buscarCelular(imeiBuscar) == false) {
            return "El imei no existe" + imeiBuscar;
        } else if (arbol.buscarCelular(imeiBuscar) != false) {
            return "El imei si existe" + imeiBuscar;
        }
        return null;
    }

    public String darPadre(String imei) {
        if (arbol.getRaiz().getDato().getImei().compareTo(imei) == 0) {
            
            return "La raiz no tiene padre";
        }
        Celular padreHoja = arbol.buscarPadre(imei);
        if (padreHoja == null) {
            
            return "No existe el imei ingresado";
        }
        return "EL padre de: "+ padre +" es: "+ padreHoja;
    }
}
