/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modeloCelulares.ArbolAVL;
import com.arbolesprog3.modeloCelulares.Celular;
import com.arbolesprog3.modeloCelulares.Marca;
import com.arbolesprog3.modeloCelulares.NodoAVL;
import com.arbolesprog3.modeloCelulares.Operador;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author daniela
 */
@Named(value = "controladorAVL")
@SessionScoped
public class ControladorAVL implements Serializable {

    private DefaultDiagramModel model;
    private ArbolAVL arbolAvl = new ArbolAVL();
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular dato = new Celular();
    private boolean verRegistrar = false;
    private boolean habilitarTabla = false;
    private boolean habilitarTablaPre = false;
    private boolean habilitarTablaIn = false;
    private boolean habilitarTablaPos = false;
    private boolean impNivel = false;

    private String operador;
    private String letra;
    private String imeiBorrar;
    private String imeiBuscar;
    private String padre;
    private boolean contarLetra = false;
    private boolean habilitarBuscar = false;
    private boolean habilitaAltura = false;
    private boolean habilitarTablaHojas = false;
    private boolean habilitaPadre = false;

    /**
     * Creates a new instance of ControladorAVL
     */
    //Constructor
    public ControladorAVL() {
    }

    //Getters and Setters
    public ArbolAVL getArbolAvl() {
        return arbolAvl;
    }

    public void setArbolAvl(ArbolAVL arbolAvl) {
        this.arbolAvl = arbolAvl;
    }

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public boolean isHabilitarTabla() {
        return habilitarTabla;
    }

    public void setHabilitarTabla(boolean habilitarTabla) {
        this.habilitarTabla = habilitarTabla;
    }

    public boolean isHabilitarTablaPre() {
        return habilitarTablaPre;
    }

    public void setHabilitarTablaPre(boolean habilitarTablaPre) {
        this.habilitarTablaPre = habilitarTablaPre;
    }

    public boolean isHabilitarTablaIn() {
        return habilitarTablaIn;
    }

    public void setHabilitarTablaIn(boolean habilitarTablaIn) {
        this.habilitarTablaIn = habilitarTablaIn;
    }

    public boolean isHabilitarTablaPos() {
        return habilitarTablaPos;
    }

    public void setHabilitarTablaPos(boolean habilitarTablaPos) {
        this.habilitarTablaPos = habilitarTablaPos;
    }

    public boolean isImpNivel() {
        return impNivel;
    }

    public void setImpNivel(boolean impNivel) {
        this.impNivel = impNivel;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getImeiBorrar() {
        return imeiBorrar;
    }

    public void setImeiBorrar(String imeiBorrar) {
        this.imeiBorrar = imeiBorrar;
    }

    public String getImeiBuscar() {
        return imeiBuscar;
    }

    public void setImeiBuscar(String imeiBuscar) {
        this.imeiBuscar = imeiBuscar;
    }

    public String getPadre() {
        return padre;
    }

    public void setPadre(String padre) {
        this.padre = padre;
    }

    public boolean isContarLetra() {
        return contarLetra;
    }

    public void setContarLetra(boolean contarLetra) {
        this.contarLetra = contarLetra;
    }

    public boolean isHabilitarBuscar() {
        return habilitarBuscar;
    }

    public void setHabilitarBuscar(boolean habilitarBuscar) {
        this.habilitarBuscar = habilitarBuscar;
    }

    public boolean isHabilitaAltura() {
        return habilitaAltura;
    }

    public void setHabilitaAltura(boolean habilitaAltura) {
        this.habilitaAltura = habilitaAltura;
    }

    public boolean isHabilitarTablaHojas() {
        return habilitarTablaHojas;
    }

    public void setHabilitarTablaHojas(boolean habilitarTablaHojas) {
        this.habilitarTablaHojas = habilitarTablaHojas;
    }

    public boolean isHabilitaPadre() {
        return habilitaPadre;
    }

    public void setHabilitaPadre(boolean habilitaPadre) {
        this.habilitaPadre = habilitaPadre;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    //metodos del arbolAVL
    @PostConstruct
    private void iniciar() {
        try {
            llenarMarcas();
            llenarOperadores();
            arbolAvl.insertarNodo(new Celular((byte) 1, "1234abcd", "3104374384", marcas.get(0), operadores.get(0), "Blanco", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 2, "5678efgh", "3104374385", marcas.get(1), operadores.get(1), "Negro", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 3, "9012ijkl", "3104374386", marcas.get(2), operadores.get(2), "Rojo", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 4, "3456mnop", "3104374387", marcas.get(3), operadores.get(0), "Gris", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 5, "7890qrst", "3104374388", marcas.get(4), operadores.get(1), "Verde", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 6, "1234uvwx", "3104374389", marcas.get(0), operadores.get(2), "Dorado", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 7, "5678yzab", "3104374380", marcas.get(1), operadores.get(0), "Azul", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 8, "9012cdef", "3104374381", marcas.get(2), operadores.get(1), "Plateado", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 9, "3456ghij", "3104374382", marcas.get(3), operadores.get(2), "Rosado", 535000, (byte) 6));
            arbolAvl.insertarNodo(new Celular((byte) 10, "7890klmn", "3104374383", marcas.get(4), operadores.get(0), "Blanco", 535000, (byte) 6));
            
            pintarArbol();
        } catch (CelularExcepcion ex) {
            Logger.getLogger(ControladorAVL.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        dato = new Celular();
    }

    public void habilitarAltura() {
        habilitaAltura = true;

    }

    public void habilitarPadre() {
        habilitaPadre = true;

    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));
        marcas.add(new Marca((short) 4, "Iphone"));
        marcas.add(new Marca((short) 5, "Motorola"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));
        operadores.add(new Operador("Tigo", "Medellin", "4G LTE"));

    }

    public void adicionarNodo() {
        try {
            arbolAvl.insertarNodo(dato);
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            dato = new Celular();
            verRegistrar = false;
            pintarArbol();

        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarInOrden() {
        try {
            arbolAvl.isLleno();
            habilitarTablaIn = true;
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolAvl.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoAVL reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco);

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            elementHijo.setStyleClass("ui-diagram-element-busc");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }

    public void balancear() {
        arbolAvl.balancear(arbolAvl.getRaiz());
        pintarArbol();

    }

    public List<Celular> recorrerPreOrdenOperador() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrdenOperador(arbolAvl.getRaiz(), listaCelulares);
        habilitarTabla = true;
        return listaCelulares;

    }

    private void recorrerPreOrdenOperador(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {
            if (reco.getDato().getOperador().getNombre().equals(this.operador)) {
                listado.add(reco.getDato());
            }

            recorrerPreOrdenOperador(reco.getIzquierda(), listado);
            recorrerPreOrdenOperador(reco.getDerecha(), listado);
        }
    }

    public void verHabilitarTabla() {
        habilitarTabla = true;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
    }

    public void verPreorden() {
        habilitarTablaPre = true;
        habilitarTablaIn = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verInOrden() {
        habilitarTablaIn = true;
        habilitarTablaPre = false;
        habilitarTablaPos = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verPosorden() {
        habilitarTablaPos = true;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTablaHojas = false;
        habilitarTabla = false;
    }

    public void verHojas() {
        habilitarTablaHojas = true;
        habilitarTablaPos = false;
        habilitarTablaPre = false;
        habilitarTablaIn = false;
        habilitarTabla = false;
    }

    public void verNiveles() {
        impNivel = true;
    }

    public void cancelarGuardado() {
        verRegistrar = false;

    }

    public void habilitarBusqueda() {
        habilitarBuscar = true;
    }

    public void habilitarContar() {
        contarLetra = true;
    }

    public int contarCelularesxLetra() {
        return arbolAvl.contarCelularesxLetra(letra);
    }

    public void borrarMenor() {
        arbolAvl.borrarMenor();
        pintarArbol();
    }

    public void borrarMayor() {
        arbolAvl.borrarMayor();
        pintarArbol();
    }

    public void borrarDato() throws CelularExcepcion {
        arbolAvl.borrarDato(getImeiBorrar());
        JsfUtil.addSuccessMessage("Se ha borrado el celular con exito");
        pintarArbol();
    }

    public void podar() {
        arbolAvl.podar();
        pintarArbol();
    }

    public String buscarCelular() {
        if (arbolAvl.buscarCelular(imeiBuscar) == false) {
            return "El imei no existe" + imeiBuscar;
        } else if (arbolAvl.buscarCelular(imeiBuscar) != false) {
            return "El imei si existe" + imeiBuscar;
        }
        return null;
    }

    public String darPadre(String imei) {
        if (arbolAvl.getRaiz().getDato().getImei().compareTo(imei) == 0) {

            return "La raiz no tiene padre";
        }
        Celular padreHoja = arbolAvl.buscarPadre(imei);
        if (padreHoja == null) {

            return "No existe el imei ingresado";
        }
        return "EL padre de: " + padre + " es: " + padreHoja;
    }
}
